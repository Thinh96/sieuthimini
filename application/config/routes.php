<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'View/index';
$route['be'] = 'BE_view/index';
$route['be/quan-ly-hinh-anh/(:any)'] = 'Uphinh/insert/$1';
$route['be/thong-tin/(:any)'] = 'Upthongtin/insert/$1';
$route['be/user/(:any)'] = 'Uprole/update/$1';
$route['be/menu/insert'] = 'Menu_C/insert';
$route['be/menu/update/(:any)'] = 'Menu_C/update/$1';
$route['be/menu/delete/(:any)'] = 'Menu_C/delete/$1';
$route['be/slider/insert'] = 'Slider_C/insert';
$route['be/slider/update/(:any)'] = 'Slider_C/update/$1';
$route['be/slider/delete/(:any)'] = 'Slider_C/delete/$1';
$route['be/danh-muc/insert'] = 'Product_type_C/insert';
$route['be/danh-muc/update/(:any)'] = 'Product_type_C/update/$1';
$route['be/danh-muc/delete/(:any)'] = 'Product_type_C/delete/$1';
$route['be/san-pham/insert'] = 'Product_C/insert';
$route['be/san-pham/update/(:any)'] = 'Product_C/update/$1';
$route['be/san-pham/delete/(:any)'] = 'Product_C/delete/$1';
$route['be/chi-tiet-san-pham/insert/(:any)'] = 'Product_detail_C/insert/$1';
$route['be/chi-tiet-san-pham/update/(:any)'] = 'Product_detail_C/update/$1';
$route['be/chi-tiet-san-pham/delete/(:any)'] = 'Product_detail_C/delete/$1';
$route['be/khuyen-mai/insert'] = 'Promotion_C/insert';
$route['be/khuyen-mai/update/(:any)'] = 'Promotion_C/update/$1';
$route['be/khuyen-mai/delete/(:any)'] = 'Promotion_C/delete/$1';
$route['be/voucher/insert'] = 'Voucher_C/insert';
$route['be/voucher/insert/(:any)'] = 'Voucher_C/insert/$1';
$route['be/voucher/update/(:any)'] = 'Voucher_C/update/$1';
$route['be/voucher/delete/(:any)'] = 'Voucher_C/delete/$1';
$route['be/tin-tuc/insert'] = 'News_C/insert';
$route['be/tin-tuc/update/(:any)'] = 'News_C/update/$1';
$route['be/tin-tuc/delete/(:any)'] = 'News_C/delete/$1';
$route['be/(:any)']="BE_view/index/$1";
$route['be/(:any)/(:any)'] = 'BE_view/index/$1/$2';
$route['dang-nhap/login'] = 'Login/log_in';
$route['dang-nhap/logout'] = 'Login/log_out';
$route['dang-ki/insert'] = 'Login/insert';
$route['user/insert2'] = 'User_C/insert2';
$route['user/update/(:any)'] = 'User_C/update/$1';
$route['user/update2'] = 'User_C/update2';
$route['gio-hang/insert/(:any)'] = 'Cart/insert/$1';
$route['gio-hang/updatevoucher'] = 'Cart/updatevoucher';
$route['gio-hang/update'] = 'Cart/update';
$route['order/insert'] = 'Order_C/insert';
$route['order/update/(:any)'] = 'Order_C/update/$1';
$route['order/delete/(:any)'] = 'Order_C/delete/$1';
$route['return/(:any)'] = 'Order_C/rt/$1';
$route['(:any)/(:any)'] = 'View/index/$1/$2';
$route['(:any)'] = 'View/index/$1';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
