<?php
class product_detail extends CI_Model {
    public function getall()
    {
        $query = $this->db->query('select * from product_detail');
        return $query->result_array();
    }
    public function insert($product,$amount,$mfg="NULL",$exp="NULL",$warr="NULL")
    {
        try{
            $query = $this->db->query('insert into product_detail(product,amount,mfg,exp,warr,created_at) values(?,?,?,?,?,NOW())',array($product,$amount,$mfg,$exp,$warr));
        }
        catch(Exception $e){
            return $e;   
        }
    }
    public function update($product,$amount,$created_at,$mfg="NULL",$exp="NULL",$warr="NULL")
    {
        if($mfg=="NULL")$mfg="is NULL";else $mfg="=".$mfg;
        if($exp=="NULL")$exp="is NULL";else $exp="=".$mfg;
        if($warr=="NULL")$warr="is NULL";else $warr="=".$mfg;
        try{
            $query = $this->db->query('update product_detail set amount=?,updated_at=NOW() where product= ? and created=? and mfg ? and exp ? and warr ?',array($amount,$product,$created_at,$mfg,$exp,$warr));
        }
        catch(Exception $e){
            return $e;   
        }
    }
    public function delete($product,$created_at,$mfg="NULL",$exp="NULL",$warr="NULL")
    {
        if($mfg=="NULL")$mfg="is NULL";else $mfg="=".$mfg;
        if($exp=="NULL")$exp="is NULL";else $exp="=".$mfg;
        if($warr=="NULL")$warr="is NULL";else $warr="=".$mfg;
        try{
            $query = $this->db->query('update product_detail set amount=0,updated_at=NOW() where product= ? and created=? and mfg ? and exp ? and warr ?',array($product,$created_at,$mfg,$exp,$warr));
        }
        catch(Exception $e)
        {return $e;}
    }
    public function getbyProduct($id)
    {
        $query = $this->db->query('select * from product_detail where product=?',array($id));
        return $query->result_array();
    }
    public function getamountbyProduct($id)
    {
        $query = $this->db->query('select SUM(amount) AS total from product_detail where product=?',array($id));
        return $query->result_array();
    }
}
?>