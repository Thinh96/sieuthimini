<?php
class voucher extends CI_Model {
    public function getall()
    {
        $query = $this->db->query('select * from voucher where amount >0');
        return $query->result_array();
    }
    public function insert($amount,$rate,$user,$from,$to)
    {
        try{
            $query = $this->db->query('insert into voucher(amount,rate,user,from_date,to_date,created_at) values(?,?,?,?,?,NOW()',array($amount,$rate,$user,$from,$to));
        }
        catch(Exception $e){
            return $e;   
        }
    }
    public function insert2($amount,$rate,$from,$to)
    {
        try{
            $query = $this->db->query('insert into voucher(amount,rate,from_date,to_date,created_at) values(?,?,?,?,NOW())',array($amount,$rate,$from,$to));
        }
        catch(Exception $e){
            return $e;   
        }
    }
    public function update($id,$amount,$rate,$user,$from,$to)
    {
        try{
            $query = $this->db->query('update voucher set amount=?,rate=?,user=?,from_date=?,to_date=?,,updated_at=NOW() where id= ?',array($id,$amount,$rate,$user,$from,$to));
        }
        catch(Exception $e){
            return $e;   
        }
    }
    public function update2($id,$amount,$rate,$from,$to)
    {
        try{
            $query = $this->db->query('update voucher set amount=?,rate=?,from_date=?,to_date=?,updated_at=NOW() where id= ?',array($amount,$rate,$from,$to,$id));
        }
        catch(Exception $e){
            return $e;   
        }
    }
    public function delete($id)
    {
        try{
            $query = $this->db->query('update voucher set amount=0,updated_at=NOW() where id= ?',array($id));
        }
        catch(Exception $e){
            return $e;   
        }
    }
    public function getbyUser($u)
    {
        $query = $this->db->query('select * from voucher where user=?,',array($u));
        return $query->result_array();
    }
    public function getNoneuser()
    {
        $query = $this->db->query('select * from voucher where user is NULL and NOW() < to_date');
        return $query->result_array();
    }
    public function getbyId($id)
    {
        $query = $this->db->query('select * from voucher where user is NULL and NOW() < to_date and id=?',array($id));
        return $query->result_array();
    }
}
?>