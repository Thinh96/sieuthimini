<?php
class display extends CI_Model {
    public function getall()
    {
        $query = $this->db->query('select * from display');
        return $query->result_array();
    }
    public function insert($name,$content)
    {
        try{
            $query = $this->db->query('insert into display(name,content) values(?,?)',array($name,$content));
        }
        catch(Exception $e){
            return $e;   
        }
    }
    public function update($name,$content)
    {
        try{
            $query = $this->db->query('update display set content = ? where name= ?',array($content,$name));
        }
        catch(Exception $e){
            return $e;   
        }
    }
    public function delete($name)
    {
        try{
            $query = $this->db->query('delete from display where name=?',array($name));
        }
        catch(Exception $e)
        {return $e;}
    }
    public function searchbyName($name)
    {
        $query = $this->db->query('select * from display where name = ?',array($name));
        return $query->result_array();
    }
    public function searchlikeName($name)
    {
        $query = $this->db->query('select * from display where name like ?',array($name.'%'));
        return $query->result_array();
    }
}
?>