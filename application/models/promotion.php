<?php
class promotion extends CI_Model {
    public function getall()
    {
        $query = $this->db->query('select * from promotion where NOW()< to_date order by rate desc, to_date asc, from_date asc, created_at');
        return $query->result_array();
    }
    public function insert($amount,$rate,$from,$to,$product)
    {
        try{
            $query = $this->db->query('insert into promotion(amount,rate,from_date,to_date,product,created_at) values(?,?,?,?,?,NOW())',array($amount,$rate,$from,$to,$product));
        }
        catch(Exception $e){
            return $e;   
        }
    }
    public function update($id,$amount,$rate,$from,$to)
    {
        try{
            $query = $this->db->query('update promotion set amount=?, rate=?, from_date=?, to_date=?, updated_at=NOW() where id=?',array($amount,$rate,$from,$to,$id));
        }
        catch(Exception $e){
            return $e;   
        }
    }
    public function delete($id)
    {
        try{
            $query = $this->db->query('update promotion set amount=0,updated_at=NOW() where id=?',array($id));
        }
        catch(Exception $e)
        {return $e;}
    }
    public function getallbyProduct($id)
    {
        $query = $this->db->query('select * from promotion where product=? and NOW() < to_date order by rate desc',array($id));
        return $query->result_array();
    }
    public function getbyId($id)
    {
        $query = $this->db->query('select * from promotion where id=? ',array($id));
        return $query->result_array();
    }
}
?>