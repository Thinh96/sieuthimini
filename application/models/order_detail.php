<?php
class order_detail extends CI_Model {
    public function getall()
    {
        $query = $this->db->query('select * from order_detail');
        return $query->result_array();
    }
    public function insert($order,$product,$amount)
    {
        try{
            $query = $this->db->query('insert into order_detail(`order`,product,amount) values(?,?,?)',array($order,$product,$amount));
        }
        catch(Exception $e){
            return $e;   
        }
    }
    public function delete($id)
    {
        try{
            $query = $this->db->query('delete from order_detail where id=?',array($id));
        }
        catch(Exception $e)
        {return $e;}
    }
    public function searchbyOrder($id)
    {
        $query = $this->db->query('select * from order_detail where `order`=?',array($id));
        return $query->result_array();
    }
}
?>