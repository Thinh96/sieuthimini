<?php
class role extends CI_Model {
    public function getall()
    {
        $query = $this->db->query('select * from role');
        return $query->result_array();
    }
    public function searchbyId($id)
    {
        $query = $this->db->query('select * from role where id=?',array($id));
        return $query->result_array();
    }
    public function searchbyName($name)
    {
        $query = $this->db->query('select * from role where name=?',array($name));
        return $query->result_array();
    }
}
?>