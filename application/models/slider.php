<?php
class slider extends CI_Model {
    public function getall()
    {
        $query = $this->db->query('select * from slider order by position');
        return $query->result_array();
    }
    public function insert($title,$small,$position)
    {
        try{
            $query = $this->db->query('insert into slider(title,small,position) values(?,?,?)',array($title,$small,$position));
        }
        catch(Exception $e){
            return $e;   
        }
    }
    public function update($id,$title,$small,$position)
    {
        try{
            $query = $this->db->query('update slider set title=?,small=?,position=? where id= ?',array($title,$small,$position,$id));
        }
        catch(Exception $e){
            return $e;   
        }
    }
    public function delete($id)
    {
        try{
            $query = $this->db->query('delete from slider where id=?',array($id));
        }
        catch(Exception $e)
        {return $e;}
    }
    public function getbyId($id)
    {
        $query = $this->db->query('select * from slider where id=?',array($id));
        return $query->result_array();
    }
    public function getbyPosition($id)
    {
        $query = $this->db->query('select * from slider where position=?',array($id));
        return $query->result_array();
    }
    public function getlast()
    {
        $query = $this->db->query('select * from slider order by position desc limit 1');
        return $query->result_array();
    }
}
?>