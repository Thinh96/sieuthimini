<?php
class product extends CI_Model {
    public function getall()
    {
        $query = $this->db->query('select * from product where status not like 0');
        return $query->result_array();
    }
    public function insert($name,$des,$price,$type,$status)
    {
        try{
            $query = $this->db->query('insert into product(name,description,price,product_type,status,created_at) values(?,?,?,?,?,NOW())',array($name,$des,$price,$type,$status));
        }
        catch(Exception $e){
            return $e;   
        }
    }
    public function update($id,$name,$des,$price,$type,$status)
    {
        try{
            $query = $this->db->query('update product set name=?,description=?,price=?,product_type=?,status=?,updated_at=NOW() where id= ?',array($name,$des,$price,$type,$status,$id));
        }
        catch(Exception $e){
            return $e;   
        }
    }
    public function delete($id)
    {
        try{
            $query = $this->db->query('update product set status=0 where id=?',array($id));
        }
        catch(Exception $e)
        {return $e;}
    }
    public function getlast()
    {
        $query = $this->db->query('select * from product order by id desc limit 1');
        return $query->result_array();
    }
    public function getbyId($id)
    {
        $query = $this->db->query('select * from product where id=?',array($id));
        return $query->result_array();
    }
    public function getnew()
    {
        $query = $this->db->query('select * from product where status=1 order by id desc');
        return $query->result_array();
    }
    public function getbyType($id)
    {
        $query = $this->db->query('select * from product where status=1 and product_type=?',array($id));
        return $query->result_array();
    }
    public function search($name)
    {
        $query = $this->db->query('select * from product where name like ? and status=1 ',array("%".$name."%"));
        return $query->result_array();
    }
}
?>