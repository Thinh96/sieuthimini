<?php
class order extends CI_Model {
    public function getall()
    {
        $query = $this->db->query('select * from `order` order by status,id desc');
        return $query->result_array();
    }
    public function insert($total,$user,$price,$voucher,$type,$status,$username='NULL')
    {
        try{
            if($voucher==0)
            {
                if($username=='NULL')
                {
                    $query = $this->db->query('insert into `order`(total,user,price,voucher,type,status,created_at) values(?,?,?,NULL,?,?,NOW())',array($total,$user,$price,$voucher,$type,$status));
                }
                else {
                    $query = $this->db->query('insert into `order`(total,user,price,voucher,type,status,created_at,username) values(?,?,?,NULL,?,?,NOW(),?)',array($total,$user,$price,$voucher,$type,$status,$username));
                }
            }
           
            else {
                if($username=='NULL')
                {
                    $query = $this->db->query('insert into `order`(total,user,price,voucher,type,status,created_at) values(?,?,?,?,?,?,NOW())',array($total,$user,$price,$voucher,$type,$status));
                }
                else {
                    $query = $this->db->query('insert into `order`(total,user,price,voucher,type,status,created_at,username) values(?,?,?,?,?,?,NOW(),?)',array($total,$user,$price,$voucher,$type,$status,$username));
                }
            }
        }
        catch(Exception $e){
            return $e;   
        }
    }
    public function update($id,$status)
    {
        try{
            $query = $this->db->query('update `order` set status=?,updated_at=NOW() where id= ?',array($status,$id));
        }
        catch(Exception $e){
            return $e;   
        }
    }
    public function delete($id)
    {
        try{
            $query = $this->db->query('update `order` set status=0 where id=?',array($id));
        }
        catch(Exception $e)
        {return $e;}
    }
    public function searchbyId($id)
    {
        $query = $this->db->query('select * from `order` where id=?',array($id));
        return $query->result_array();
    }
    public function getlast()
    {
        $query = $this->db->query('select * from `order` order by id desc limit 1');
        return $query->result_array();
    }
    public function getallUser($id)
    {
        $query = $this->db->query('select * from `order` where user=?',array($id));
        return $query->result_array();
    }
    public function getallUsername($id)
    {
        $query = $this->db->query('select * from `order` where username=?',array($id));
        return $query->result_array();
    }
    public function getnew()
    {
        $query = $this->db->query('select * from `order` where status not like 0 and status not like 4 order by id desc');
        return $query->result_array();
    }
}
?>