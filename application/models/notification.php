<?php
class notification extends CI_Model {
    public function getall()
    {
        $query = $this->db->query('select * from notification');
        return $query->result_array();
    }
    public function insert($username,$content,$order)
    {
        try{
            $query = $this->db->query('insert into notification(username,content,order,created_at) values(?,?,?,,NOW())',array($username,$content,$order));
        }
        catch(Exception $e){
            return $e;   
        }
    }
}
?>