<?php
class user extends CI_Model {
    public function getall()
    {
        $query = $this->db->query('select * from user');
        return $query->result_array();
    }
    public function insert($name,$gender,$birthday,$address,$phone,$mail,$idcard,$role,$username="NULL",$password="NULL")
    {
        try{
            if($username=="NULL" || $password=="NULL")
            {
                $query = $this->db->query('insert into user(name,gender,birthday,address,phone,email,id_card,role,username,password) values(?,?,?,?,?,?,?,?,NULL,NULL)',array($name,$gender,$birthday,$address,$phone,$mail,$idcard,$role));
            }
            else
            $query = $this->db->query('insert into user(name,gender,birthday,address,phone,email,id_card,role,username,password) values(?,?,?,?,?,?,?,?,?,?)',array($name,$gender,$birthday,$address,$phone,$mail,$idcard,$role,$username,$password));
        }
        catch(Exception $e){
            return $e;   
        }
    }
    public function update1($username,$password)
    {
        try{
            $query = $this->db->query('update user set password=?,updated_at=NOW() where username= ?',array($password,$username));
        }
        catch(Exception $e){
            return $e;   
        }
    }
    public function update2($name,$gender,$birthday,$address,$phone,$mail,$idcard,$id)
    {
        try{
            $query = $this->db->query('update user set name=?,gender=?,birthday=?,address=?,phone=?,email=?,id_card=?,updated_at=NOW() where id= ?',array($name,$gender,$birthday,$address,$phone,$mail,$idcard,$id));
        }
        catch(Exception $e){
            return $e;   
        }
    }
    public function update3($role,$id)
    {
        try{
            $query = $this->db->query('update user set role=?,updated_at=NOW() where id= ?',array($role,$id));
        }
        catch(Exception $e){
            return $e;   
        }
    }
    public function getallbyRole($id)
    {
        $query = $this->db->query('select * from user where role=?',array($id));
        return $query->result_array();
    }
    public function getlast()
    {
        $query = $this->db->query('select * from user order by id desc limit 1');
        return $query->result_array();
    }
    public function getbyId($id)
    {
        $query = $this->db->query('select * from user where id=?',array($id));
        return $query->result_array();
    }
    public function getbyUsername($username)
    {
        $query = $this->db->query('select * from user where username=?',array($username));
        return $query->result_array();
    }
    public function searchbyName_Role($name,$role)
    {
        $query = $this->db->query('select * from user where role=? and name like',array($role,"%".$name,"%"));
        return $query->result_array();
    }
    public function checklogin($username,$password)
    {
        $query = $this->db->query('select * from user where password=?',array(sha1(md5($username.$password))));
        return $query->result_array();
    }
}
?>