<?php
class menu extends CI_Model {
    public function getall()
    {
        $query = $this->db->query('select * from menu order by position');
        return $query->result_array();
    }
    public function insert($name,$link,$position)
    {
        try{
            $query = $this->db->query('insert into menu(name,link,position) values(?,?,?)',array($name,$link,$position));
        }
        catch(Exception $e){
            return $e;   
        }
    }
    public function update($id,$name,$link,$position)
    {
        try{
            $query = $this->db->query('update menu set name=?,link=?,position=? where id= ?',array($name,$link,$position,$id));
        }
        catch(Exception $e){
            return $e;   
        }
    }
    public function delete($id)
    {
        try{
            $query = $this->db->query('delete from menu where id=?',array($id));
        }
        catch(Exception $e)
        {return $e;}
    }
    public function getbyId($id)
    {
        $query = $this->db->query('select * from menu where id=?',array($id));
        return $query->result_array();
    }
    public function getbyPosition($id)
    {
        $query = $this->db->query('select * from menu where position=?',array($id));
        return $query->result_array();
    }
    public function searchlikeName($link)
    {
        $query = $this->db->query('select * from menu where link=?',array($link));
        return $query->result_array();
    }
    public function getlast()
    {
        $query = $this->db->query('select * from menu order by id desc limit 1');
        return $query->result_array();
    }
}
?>