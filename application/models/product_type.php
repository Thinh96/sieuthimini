<?php
class product_type extends CI_Model {
    public function getall()
    {
        $query = $this->db->query('select * from product_type');
        return $query->result_array();
    }
    public function getlast()
    {
        $query = $this->db->query('select * from product_type order by id desc limit 1');
        return $query->result_array();
    }
    public function insert($name,$parent_id,$status)
    {
        try{
            if($parent_id=="NULL"){
                $query = $this->db->query('insert into product_type(name,status,created_at) values(?,?,NOW())',array($name,$status));
            }
            else{
                $query = $this->db->query('insert into product_type(name,parent_id,status,created_at) values(?,?,?,NOW())',array($name,$parent_id,$status));
            }
        }
        catch(Exception $e){
            return $e;   
        }
    }
    public function update($id,$name,$parent_id,$status)
    {
        try{
            if($parent_id=='NULL') 
            {
                $query = $this->db->query('update product_type set name=?,parent_id=NULL,status=?,updated_at=NOW() where id= ?',array($name,$status,$id));
            }
            else{
                $query = $this->db->query('update product_type set name=?,parent_id=?,status=?,updated_at=NOW() where id= ?',array($name,$parent_id,$status,$id));
            }
        }
        catch(Exception $e){
            return $e;   
        }
    }
    public function delete($id)
    {
        try{
            $query = $this->db->query('delete from product_type where id=?',array($id));
        }
        catch(Exception $e)
        {return $e;}
    }
    public function getbyId($id)
    {
        $query = $this->db->query('select * from product_type where id=?',array($id));
        return $query->result_array();
    }
    public function getallParent()
    {
        $query = $this->db->query('select * from product_type where parent_id is NULL');
        return $query->result_array();
    }
    public function getallbyParent($id)
    {
        $query = $this->db->query('select * from product_type where parent_id = ?',array($id));
        return $query->result_array();
    }
    public function searchbyName($name)
    {
        $query = $this->db->query('select * from product_type where name=?',array($name));
        return $query->result_array();
    }
    public function searchlikePName($name)
    {
        $query = $this->db->query('select * from product_type where name like ? and parent_id is NULL',array("%".$name."%"));
        return $query->result_array();
    }
    public function searchlikeName($name)
    {
        $query = $this->db->query('select * from product_type where name like ?',array("%".$name."%"));
        return $query->result_array();
    }
}
?>