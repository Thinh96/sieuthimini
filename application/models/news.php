<?php
class news extends CI_Model {
    public function getall()
    {
        $query = $this->db->query('select * from news');
        return $query->result_array();
    }
    public function getlast()
    {
        $query = $this->db->query('select * from news order by id desc limit 1');
        return $query->result_array();
    }
    public function insert($title,$content,$username,$status)
    {
        try{
            $query = $this->db->query('insert into news(title,content,username,status,created_at) values(?,?,?,?,NOW())',array($title,$content,$username,$status));
        }
        catch(Exception $e){
            return $e;   
        }
    }
    public function update($id,$title,$content,$username,$status)
    {
        try{
            $query = $this->db->query('update news set title=?,content=?,username=?,status=?,updated_at=NOW() where id= ?',array($title,$content,$username,$status,$id));
        }
        catch(Exception $e){
            return $e;   
        }
    }
    public function delete($id)
    {
        try{
            $query = $this->db->query('update news set status=0 where id=?',array($id));
        }
        catch(Exception $e)
        {return $e;}
    }
    public function getbyId($id)
    {
        $query = $this->db->query('select * from news where id=?',array($id));
        return $query->result_array();
    }
    public function searchbyTitle($title)
    {
        $query = $this->db->query('select * from news where title=?',array($title));
        return $query->result_array();
    }
}
?>