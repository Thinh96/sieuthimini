<?php
class logs extends CI_Model {
    public function getall()
    {
        $query = $this->db->query('select * from log order by id desc');
        return $query->result_array();
    }
    public function insert($user,$action,$type,$id_type)
    {
        try{
            $query = $this->db->query('insert into log(user,action,type,id_type,created_at) values(?,?,?,?,NOW())',array($user,$action,$type,$id_type));
        }
        catch(Exception $e){
            return $e;   
        }
    }
    public function searchbyUser($user)
    {
        $query = $this->db->query('select * from display where user = ?',array($user));
        return $query->result_array();
    }
}
?>