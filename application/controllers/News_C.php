<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class News_C extends CI_Controller {
	public function insert($id=0)
	{
        if(!isset($_POST['title']) || !isset($_POST['username']) || !isset($_POST['content']) )
        {
            redirect(base_url()."be/them-tin-tuc?err=Cần%20nhập%20thông%20tin%20đầy%20đủ");
        }
        $news=new news();
        if($id==0){
            $rt=$news->insert($_POST['title'],$_POST['content'],$_POST['username'],1);
        }
        if($rt!='')
            {
                redirect(base_url()."be/that-bai?page=tin-tuc&err=$rt");
            }
            else
            {
                $log=new logs();
                $last=$news->getlast();
                $log->insert($_SESSION['user_data']['username'],"Đã thêm","Tin tức",$last[0]['id']);
                 redirect(base_url()."be/thanh-cong?page=tin-tuc");
            }
    }
    public function delete($id=0)
	{
        if($id==0)
        {redirect(base_url()."be/quan-ly-tin-tuc");}
        $news=new news();
        $rt=$news->delete($id);
        if($rt!='')
        {
            redirect(base_url()."be/that-bai?page=tin-tuc&err=$rt");
        }
        else
        {
            $log=new logs();
            $log->insert($_SESSION['user_data']['username'],"Đã xóa","Tin tức",$id);
            redirect(base_url()."be/thanh-cong?page=tin-tuc");
        }
    }
    public function update($id=0)
	{
        if($id==0)
        {redirect(base_url()."be/quan-ly-tin-tuc");}
        $news=new news();
        $rt=$news->update($id,$_POST['title'],$_POST['content'],$_POST['username'],$_POST['status']);
        if($rt!='')
        {
            redirect(base_url()."be/that-bai?page=tin-tuc&id=$id&err=$rt");
        }
        else
        {
            $log=new logs();
            $log->insert($_SESSION['user_data']['username'],"Đã cập nhật","Tin tức",$id);
            redirect(base_url()."be/thanh-cong?page=tin-tuc&id=$id");
        }
    }
}
?>