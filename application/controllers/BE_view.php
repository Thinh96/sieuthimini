<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class BE_view extends CI_Controller {
	public function index($p='trang-chu',$id='0')
	{
        if ( ! file_exists(APPPATH.'views/be/pages/'.$p.'.php'))
        {
                show_404();
        }
        if(!isset($_SESSION['user_data']['role']))
        {
                show_404();
        }
        if($_SESSION['user_data']['role']<3)
        {
                show_404();
        }
        $page=array(
            'trang-chu'=>'Trang chủ',
            'quan-ly-giao-dien'=>'Quản lý giao diện',
            'quan-ly-menu'=>'Quản lý menu',
            'menu'=>'Menu',
            'them-menu'=>'Thêm menu',
            'quan-ly-slider'=>'Quản lý slider',
            'slider'=>'Slider',
            'them-slider'=>'Thêm slider',
            'quan-ly-hinh-anh'=>'Quản lý hình ảnh',
            'hinh-anh'=>'Hình ảnh',
            'quan-ly-thong-tin-website'=>'Quản lý thông tin website',
            'thong-tin-website'=>'Thông tin website',
            'quan-ly-danh-muc'=>'Quản lý danh mục',
            'danh-muc'=>'Danh mục',
            'them-danh-muc'=>'Thêm danh mục',
            'quan-ly-san-pham'=>'Quản lý sản phẩm',
            'san-pham'=>'Sản phẩm',
            'them-san-pham'=>'Thêm sản phẩm',
            'chi-tiet-san-pham'=>'Chi tiết sản phẩm',
            'them-chi-tiet-san-pham'=>'Thêm chi tiết sản phẩm',
            'quan-ly-hoa-don'=>'Quản lý hóa đơn',
            'hoa-don'=>'Hóa đơn',
            'quan-ly-tin-tuc'=>'Quản lý tin tức',
            'tin-tuc'=>'Tin tức',
            'them-tin-tuc'=>'Thêm tin tức',
            'quan-ly-khuyen-mai'=>'Quản lý khuyến mại',
            'khuyen-mai'=>'Khuyến mại',
            'them-khuyen-mai'=>'Thêm khuyến mại',
            'quan-ly-voucher'=>'Quản lý voucher',
            'voucher'=>'Voucher',
            'them-voucher'=>'Thêm voucher',
            'quan-ly-khach-hang'=>'Quản lý khách hàng',
            'khach-hang'=>'Khách hàng',
            'quan-ly-nhan-vien'=>'Quản lý nhân viên',
            'nhan-vien'=>'Nhân viên',
            'them-nhan-vien'=>'Thêm nhân viên',
            'thanh-cong'=>'Thông báo thành công',
            'that-bai'=>'Thông báo thất bại'
        );
        $data['title']=$page[$p];
        $data['page']=$p;
        $data['id']=$id;
       $this->load->view('be/templates/header',$data);
        $this->load->view('be/templates/vmenu',$data);
        $this->load->view('be/templates/sidebar',$data);
        $this->load->view('be/pages/'.$p,$data);
        $this->load->view('be/templates/footer',$data);
	}
}