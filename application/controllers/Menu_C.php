<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Menu_C extends CI_Controller {
	public function insert()
	{
        if(!isset($_POST['submit']) || !isset($_POST['name']))
        {
            redirect(base_url()."be/them-menu");
        }
        $menu=new menu();
        $this->load->helper('text');
        function url_rl($str)
        {
            $str=convert_accented_characters(($str));
            $str=str_replace(" ","-",$str);
            $str=strtolower($str);
            return $str;
        }
        $link=url_rl($_POST['name']);
        $position=count($menu->getall())+1;
        $rt=$menu->insert($_POST['name'],$link,$position);
        if($rt!='')
        {
            redirect(base_url()."be/that-bai?page=menu&err=$rt");
        }
        else
        {
            $log=new logs();
            $last=$menu->getlast();
            $log->insert($_SESSION['user_data']['username'],"Đã thêm","Menu",$last[0]['id']);
            redirect(base_url()."be/thanh-cong?page=menu");
        }
    }
    public function delete($id=0)
	{
        if($id==0)
        {redirect(base_url()."be/quan-ly-menu");}
        $menu=new menu();
        $rt=$menu->delete($id);
        if($rt!='')
        {
            redirect(base_url()."be/that-bai?page=menu&err=$rt");
        }
        else
        {
            $log=new logs();
            $log->insert($_SESSION['user_data']['username'],"Đã xóa","Menu",$id);
            redirect(base_url()."be/thanh-cong?page=menu");
        }
    }
    public function update($id=0)
	{
        if($id==0)
        {redirect(base_url()."be/quan-ly-menu");}
        $menu=new menu();
        $this->load->helper('text');
        function url_rl($str)
        {
            $str=convert_accented_characters(($str));
            $str=str_replace(" ","-",$str);
            $str=strtolower($str);
            return $str;
        }
        $link=url_rl($_POST['name']);
        $mn1=$menu->getbyId($id);
        $mn2=$menu->getbyPosition($_POST['position']);
        if($mn1[0]['id']==$mn2[0]['id'])
            {$rt=$menu->update($id,$_POST['name'],$link,$_POST['position']);}
        else{
            $rt=$menu->update($id,$_POST['name'],$link,$_POST['position']);
            $rt=$menu->update($mn2[0]['id'],$mn2[0]['name'],$mn2[0]['link'],$mn1[0]['position']);
        }
        if($rt!='')
        {
            redirect(base_url()."be/that-bai?page=menu&err=$rt");
        }
        else
        {
            $log=new logs();
            $log->insert($_SESSION['user_data']['username'],"Đã cập nhật","Menu",$id);
            redirect(base_url()."be/thanh-cong?page=menu");
        }
    }
}
?>