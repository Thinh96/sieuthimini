<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Product_C extends CI_Controller {
	public function insert()
	{
        if(!isset($_POST['submit']) || !isset($_POST['name'])|| !isset($_POST['type'])|| !isset($_POST['price']) || !isset($_POST['des']) || !isset($_FILES['hinh']))
        {
            redirect(base_url()."be/them-san-pham?err=Cần%20nhập%20thông%20tin%20đầy%20đủ");
        }
        if($_POST['name']=="" ||$_POST['price']==""||$_POST['type']=="" ||$_POST['des']=="")
        {
            redirect(base_url()."be/them-san-pham?err=Cần%20nhập%20thông%20tin%20đầy%20đủ");
        }
        $file_type=$_FILES['hinh']['type'];
        $expensions= array("image/jpeg","image/jpg","image/png");
				if(in_array($file_type,$expensions)== false){
					redirect(base_url()."be/them-san-pham?err=Hình%20không%20đúng%20định%20dạng");
				}
        $product=new product();
        $rt=$product->insert($_POST['name'],$_POST['des'],$_POST['price'],$_POST['type'],1);
        $sd=$product->getlast();
        $id=$sd[0]['id'];
                    $config['overwrite']            =true;
					$config['upload_path']          = './image/product';
					$config['allowed_types']        = '*';
					$config['file_name']            =$id.".png";
                	$config['max_width']            = 2048;
                	$config['max_height']           = 1500;
                	$this->load->library('upload', $config);
                	if ( ! $this->upload->do_upload('hinh'))
                	{
                        $error = array('error' => $this->upload->display_errors());
                    }
        if($rt!='')
            {
                redirect(base_url()."be/that-bai?page=san-pham&err=$rt");
            }
            else
            {
                $log=new logs();
                $last=$product->getlast();
                $log->insert($_SESSION['user_data']['username'],"Đã thêm","sản phẩm",$last[0]['id']);
                 redirect(base_url()."be/thanh-cong?page=san-pham");
            }
    }
    public function delete($id=0)
	{
        if($id==0)
        {redirect(base_url()."be/quan-ly-san-pham");}
        $product=new product();
        $rt=$product->delete($id);
        if($rt!='')
        {
            redirect(base_url()."be/that-bai?page=san-pham&err=$rt");
        }
        else
        {
            $log=new logs();
            $log->insert($_SESSION['user_data']['username'],"Đã xóa","Sản phẩm",$id);
            redirect(base_url()."be/thanh-cong?page=san-pham");
        }
    }
    public function update($id=0)
	{
        if($id==0)
        {redirect(base_url()."be/quan-ly-san-pham");}
        if(!isset($_POST['submit']) || !isset($_POST['name'])|| !isset($_POST['type'])|| !isset($_POST['price']) || !isset($_POST['des']))
        {
            redirect(base_url()."be/san-pham/$id?err=Cần%20nhập%20thông%20tin%20đầy%20đủ");
        }
        if($_POST['name']=="" ||$_POST['price']==""||$_POST['type']=="" ||$_POST['des']=="")
        {
            redirect(base_url()."be/san-pham/$id?err=Cần%20nhập%20thông%20tin%20đầy%20đủ");
        }
        if(isset($_FILES['hinh']['name']) && $_FILES['hinh']['name']!="")
        {
            $file_type=$_FILES['hinh']['type'];
            $expensions= array("image/jpeg","image/jpg","image/png");
                    if(in_array($file_type,$expensions)== false){
                        redirect(base_url()."be/san-pham/$id?err=Hình%20không%20đúng%20định%20dạng");
                    }
                    $config['overwrite']            =true;
					$config['upload_path']          = './image/product';
					$config['allowed_types']        = '*';
					$config['file_name']            =$id.".png";
                	$config['max_width']            = 2048;
                	$config['max_height']           = 1500;
                	$this->load->library('upload', $config);
                	if ( ! $this->upload->do_upload('hinh'))
                	{
                        $error = array('error' => $this->upload->display_errors());
                    }
        }
        $product=new product();
        $rt=$product->update($id,$_POST['name'],$_POST['des'],$_POST['price'],$_POST['type'],$_POST['status']);
        if($rt!='')
            {
                redirect(base_url()."be/that-bai?page=san-pham&err=$rt");
            }
            else
            {
                $log=new logs();
                $log->insert($_SESSION['user_data']['username'],"Đã cập nhật","sản phẩm",$id);
                 redirect(base_url()."be/thanh-cong?page=san-pham");
            }
    }
}
?>