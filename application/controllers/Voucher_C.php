<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Voucher_C extends CI_Controller {
	public function insert($id=0)
	{
        if(!isset($_POST['amount']) || !isset($_POST['rate']) || !isset($_POST['from']) || !isset($_POST['to']) )
        {
            redirect(base_url()."be/them-voucher?err=Cần%20nhập%20thông%20tin%20đầy%20đủ");
        }
        if($_POST['from']>$_POST['to'])
        {
            redirect(base_url()."be/them-voucher?err=Ngày%20bắt%20không%20được%20sau%20ngày%20kết%20thúc");
        }
        $voucher=new voucher();
        if($id==0){
            $rt=$voucher->insert2($_POST['amount'],$_POST['rate'],$_POST['from'],$_POST['to']);
        }
        if($rt!='')
            {
                redirect(base_url()."be/that-bai?page=voucher&err=$rt");
            }
            else
            {
                $log=new logs();
                $log->insert($_SESSION['user_data']['username'],"Đã thêm","voucher",0);
                 redirect(base_url()."be/thanh-cong?page=voucher");
            }
    }
    public function delete($id=0)
	{
        if($id==0)
        {redirect(base_url()."be/quan-ly-voucher");}
        $voucher=new voucher();
        $rt=$voucher->delete($id);
        if($rt!='')
        {
            redirect(base_url()."be/that-bai?page=voucher&err=$rt");
        }
        else
        {
            $log=new logs();
            $log->insert($_SESSION['user_data']['username'],"Đã xóa","voucher",$id);
            redirect(base_url()."be/thanh-cong?page=voucher");
        }
    }
    public function update($id=0)
	{
        if($id==0)
        {redirect(base_url()."be/quan-ly-voucher");}
        $voucher=new voucher();
        $rt=$voucher->update2($id,$_POST['amount'],$_POST['rate'],$_POST['from'],$_POST['to']);
        if($rt!='')
        {
            redirect(base_url()."be/that-bai?page=voucher&id=$id&err=$rt");
        }
        else
        {
            $log=new logs();
            $log->insert($_SESSION['user_data']['username'],"Đã cập nhật","voucher",$id);
            redirect(base_url()."be/thanh-cong?page=voucher&id=$id");
        }
    }
}
?>