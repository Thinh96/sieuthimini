<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class View extends CI_Controller {
	public function index($p='trang-chu',$id=0)
	{
        if ( ! file_exists(APPPATH.'views/fe/pages/'.$p.'.php'))
        {
                show_404();
        }
        $page=array(
            'trang-chu'=>'Trang chủ',
            'dang-nhap'=>'Đăng nhập',
            'dang-ki'=>'Trang chủ',
            'danh-sach-san-pham'=>'Danh sách sản phẩm',
            'san-pham'=>'Chi tiết sản phẩm',
            'danh-sach-tin-tuc'=>'Danh sách tin tức',
            'tin-tuc'=>'Chi tiết tin tức',
            'gio-hang'=>'Giỏ hàng',
            'dang-ki-thanh-cong'=>'Đăng kí thành công',
            'dang-nhap-thanh-cong'=>'Đăng nhập thành công',
            'thong-tin-ca-nhan'=>'Thông tin cá nhân',
            'cap-nhat-thong-tin'=>'Cập nhật thông tin',
            'danh-sach-don-hang'=>'Dang sách đơn hàng',
            'thanh-toan'=>'Thanh toán',
            'lien-he'=>'Liên hệ',
            'cau-hoi-thuong-gap'=>'Câu hỏi thường gặp',
            'doi-mat-khau'=>'Đổi mật khẩu'
        );
        if(!isset($_SESSION['user_data']))
        {
            $user_data=array(
                'username'=>"",
                "role"=>"1",
                "cart"=>array(
                    "amount"=>0,
                    "list"=>array(),
                    "total"=>0,
                    "voucher"=>0
                )
            );
            $_SESSION['user_data']=$user_data;
            $display=new display();
            $d_view=$display->searchbyName('daily view');
            $t_view=$display->searchbyName('total view');
            $display->update('daily view',$d_view[0]['content']+1);
            $display->update('total view',$t_view[0]['content']+1);
        }
        $data['title']=$page[$p];
        $data['id']=$id;
        $this->load->view('fe/template/header',$data);
        $this->load->view('fe/template/menu',$data);
        $this->load->view('fe/pages/'.$p,$data);
        $this->load->view('fe/template/footer',$data);
	}
}
