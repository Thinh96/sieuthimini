<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Slider_C extends CI_Controller {
	public function insert()
	{
        if(!isset($_POST['submit']) || !isset($_POST['title']) || !isset($_POST['small']) || !isset($_FILES['hinh']))
        {
            redirect(base_url()."be/them-slider");
        }
        $slider=new slider();
        $position=count($slider->getall())+1;
        $rt=$slider->insert($_POST['title'],$_POST['small'],$position);
        $sd=$slider->getlast();
        $id=$sd[0]['id'];
                    $config['overwrite']=true;
					$config['upload_path']          = './image/';
					$config['allowed_types']        = '*';
					$config['file_name']="slider_".$id.".png";
                	$config['max_size']             = 10000;
                	$config['max_width']            = 2048;
                	$config['max_height']           = 1500;
                	$this->load->library('upload', $config);
                	if ( ! $this->upload->do_upload('hinh'))
                	{
                        $error = array('error' => $this->upload->display_errors());
                    }
        if($rt!='')
            {
                redirect(base_url()."be/that-bai?page=slider&err=$rt");
            }
            else
            {
                 redirect(base_url()."be/thanh-cong?page=slider");
            }
    }
    public function delete($id=0)
	{
        $this->load->helper('file');
        if($id==0)
        {redirect(base_url()."be/quan-ly-slider");}
        $slider=new slider();
        unlink('image/slider_'.$id.'.png');
        $rt=$slider->delete($id);
        if($rt!='')
        {
            redirect(base_url()."be/that-bai?page=slider&err=$rt");
        }
        else
        {
            redirect(base_url()."be/thanh-cong?page=slider");
        }
    }
    public function update($id=0)
	{
        if($id==0)
        {redirect(base_url()."be/quan-ly-slider");}
        $slider=new slider();
        $sd1=$slider->getbyId($id);
        $sd2=$slider->getbyPosition($_POST['position']);
        if($sd1[0]['id']==$sd2[0]['id'])
            {$rt=$slider->update($id,$_POST['title'],$_POST['small'],$_POST['position']);}
        else{
            $rt=$slider->update($id,$_POST['title'],$_POST['small'],$_POST['position']);
            $rt=$slider->update($sd2[0]['id'],$sd2[0]['title'],$sd2[0]['small'],$sd1[0]['position']);
        }
        if(isset($_FILES['hinh']))
        {
            $config['overwrite']=true;
            $config['upload_path']          = './image/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['file_name']="slider_".$id.".png";
            $config['max_size']             = 10000;
            $config['max_width']            = 2048;
            $config['max_height']           = 1500;
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('hinh'))
            {
                $error = array('error' => $this->upload->display_errors());
            }
        }
        if($rt!='')
        {
            redirect(base_url()."be/that-bai?page=slider&err=$rt");
        }
        else
        {
            redirect(base_url()."be/thanh-cong?page=slider");
        }
    }
}
?>