<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Promotion_C extends CI_Controller {
	public function insert()
	{
        if(!isset($_POST['amount']) || !isset($_POST['rate']) || !isset($_POST['from']) || !isset($_POST['to']) || !isset($_POST['id']))
        {
            redirect(base_url()."be/them-khuyen-mai?err=Cần%20nhập%20thông%20tin%20đầy%20đủ");
        }
        if($_POST['from']>$_POST['to'])
        {
            redirect(base_url()."be/them-khuyen-mai?err=Ngày%20bắt%20không%20được%20sau%20ngày%20kết%20thúc");
        }
        $p=new product();
        $pd=$p->getbyid($_POST['id']);
        if($pd[0]['status']!=1)
        {
            redirect(base_url()."be/them-khuyen-mai?err=San%20phẩm%20không%20thể%20thêm%20khuyến%20mại");
        }
        $pro=new promotion();
        $rt=$pro->insert($_POST['amount'],$_POST['rate'],$_POST['from'],$_POST['to'],$_POST['id']);
        if($rt!='')
            {
                redirect(base_url()."be/that-bai?page=khuyen-mai&err=$rt");
            }
            else
            {
                $log=new logs();
                $log->insert($_SESSION['user_data']['username'],"Đã thêm","khuyến mại",0);
                 redirect(base_url()."be/thanh-cong?page=khuyen-mai");
            }
    }
    public function delete($id=0)
	{
        if($id==0)
        {redirect(base_url()."be/quan-ly-khuyen-mai");}
        $pro=new promotion();
        $rt=$pro->delete($id);
        if($rt!='')
        {
            redirect(base_url()."be/that-bai?page=khuyen-mai&err=$rt");
        }
        else
        {
            $log=new logs();
            $log->insert($_SESSION['user_data']['username'],"Đã xóa","khuyến mại",$id);
            redirect(base_url()."be/thanh-cong?page=khuyen-mai");
        }
    }
    public function update($id=0)
	{
        if($id==0)
        {redirect(base_url()."be/quan-ly-khuyen-mai");}
        $pro=new promotion();
        $rt=$pro->update($id,$_POST['amount'],$_POST['rate'],$_POST['from'],$_POST['to']);
        if($rt!='')
        {
            redirect(base_url()."be/that-bai?page=khuyen-mai&id=$id&err=$rt");
        }
        else
        {
            $log=new logs();
            $log->insert($_SESSION['user_data']['username'],"Đã cập nhật","khuyến mại",$id);
            redirect(base_url()."be/thanh-cong?page=khuyen-mai&id=$id");
        }
    }
}
?>