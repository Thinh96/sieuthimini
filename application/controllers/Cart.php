<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cart extends CI_Controller {
	public function insert($id=0)
	{
        if($id==0)
        {
            redirect(base_url());
        }
        $p=new product();
        $pd=$p->getbyId($id);
        $pro=new promotion();
        $rate=0;
        if($pro->getallbyProduct($id)>0);
        $rate=$pro->getallbyProduct($id)[0]['rate'];
        if(!isset($_POST['amount'])){
            $f=-1;
            foreach($_SESSION['user_data']['cart']['list'] as $k=>$v)
            {
                if($v['id']==$id) $f=$k;
            }
            if($f>=0)
            {
                $_SESSION['user_data']['cart']['total']+=round($pd[0]['price']*(1-$rate/100),-3);
                $_SESSION['user_data']['cart']['list'][$f]['amount']++;
            }
            else {
                $_SESSION['user_data']['cart']['amount']++;
                $_SESSION['user_data']['cart']['total']+=round($pd[0]['price']*(1-$rate/100),-3);
                $_SESSION['user_data']['cart']['list'][]=array('id'=>$id,'amount'=>1);
            }
        }
        else{
            $f=0;
            foreach($_SESSION['user_data']['cart']['list'] as $k=>$v)
            {
                if($v['id']==$id) $f=$k;
            }
            if($f>=0)
            {
                $_SESSION['user_data']['cart']['total']+=round($pd[0]['price']*$_POST['amount'],-3);
                $_SESSION['user_data']['cart']['list'][$f]['amount']+=$_POST['amount'];
            }
            else {
                $_SESSION['user_data']['cart']['amount']++;
                $_SESSION['user_data']['cart']['total']+=round($pd[0]['price']*$_POST['amount'],-3);
                $_SESSION['user_data']['cart']['list'][]=array('id'=>$id,'amount'=>$_POST['amount']);
            }
        }
        redirect(base_url().'gio-hang');
    }
    public function updatevoucher()
	{
        if(isset($_GET['voucher'])){
            $voucher=new voucher();
            if(count($voucher->getbyId($_GET['voucher']))>0){$_SESSION['user_data']['cart']['voucher']=$_GET['voucher'];}
        }
        redirect(base_url().'gio-hang');
    }
    public function update()
	{
        $p=new product();
        $total=0;
		
        foreach($_POST['num-product'] as $k=> $v)
        {
            $_SESSION['user_data']['cart']['list'][$k]['amount']=$v;
            $product=$p->getbyId($_SESSION['user_data']['cart']['list'][$k]['id']);
            $pro=new promotion();
            $rate=0;
            if($pro->getallbyProduct($product[0]['id'])>0);
            $rate=$pro->getallbyProduct($product[0]['id'])[0]['rate'];
            $total+=round($product[0]['price']*$v*(1-$rate/100),-3);
        }
        foreach($_SESSION['user_data']['cart']['list'] as $k=>$v)
        {
            if($_SESSION['user_data']['cart']['list'][$k]['amount']==0) 
            {
                unset($_SESSION['user_data']['cart']['list'][$k]);
                $_SESSION['user_data']['cart']['amount']--;
            }
        }
        $_SESSION['user_data']['cart']['total']=$total;
        redirect(base_url().'gio-hang');
    }
}
?>