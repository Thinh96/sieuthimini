<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Product_detail_C extends CI_Controller {
	public function insert($id)
	{
        if(!isset($_POST['amount']) )
        {
            redirect(base_url()."be/them-chi-tiet-san-pham?err=Cần%20nhập%20số%20lượng");
        }
        if($_POST['amount']=='')
        {
            redirect(base_url()."be/them-chi-tiet-san-pham?err=Cần%20nhập%20thông%20tin%20đầy%20đủ");
        }
        if($_POST['mfg']!='' &&  $_POST['exp']!='' && $_POST['mfg'] > $_POST['exp'])
        {
            redirect(base_url()."be/them-chi-tiet-san-pham?id=$id&  err=Hạn%20sử%20dụng%20phải%20lớn%20hơn%20ngày%20sản%20xuất");
        }
        $p_d=new product_detail();
        $rt=$p_d->insert($id,$_POST['amount'],$_POST['mfg'],$_POST['exp'],$_POST['warr'],1);
        if($rt!='')
            {
                redirect(base_url()."be/that-bai?page=san-pham&id=$id&err=$rt");
            }
            else
            {
                $log=new logs();
                $log->insert($_SESSION['user_data']['username'],"Đã cập nhật","sản phẩm",$id);
                 redirect(base_url()."be/thanh-cong?page=san-pham&id=$id");
            }
    }
    public function delete($id=0)
	{
        if($id==0)
        {redirect(base_url()."be/quan-ly-san-pham");}
        $product=new product();
        $rt=$product->delete($id);
        if($rt!='')
        {
            redirect(base_url()."be/that-bai?page=san-pham&err=$rt");
        }
        else
        {
            $log=new logs();
            $log->insert($_SESSION['user_data']['username'],"Đã cập nhật","sản phẩm",$id);
            redirect(base_url()."be/thanh-cong?page=san-pham");
        }
    }
    public function update($id=0)
	{
        if($id==0)
        {redirect(base_url()."be/quan-ly-san-pham");}
        if(!isset($_POST['submit']) || !isset($_POST['name'])|| !isset($_POST['type'])|| !isset($_POST['price']) || !isset($_POST['des']))
        {
            redirect(base_url()."be/san-pham/$id?err=Cần%20nhập%20thông%20tin%20đầy%20đủ");
        }
        if($_POST['name']=="" ||$_POST['price']==""||$_POST['type']=="" ||$_POST['des']=="")
        {
            redirect(base_url()."be/san-pham/$id?err=Cần%20nhập%20thông%20tin%20đầy%20đủ");
        }
        if(isset($_FILES['hinh']['name']) && $_FILES['hinh']['name']!="")
        {
            $file_type=$_FILES['hinh']['type'];
            $expensions= array("image/jpeg","image/jpg","image/png");
                    if(in_array($file_type,$expensions)== false){
                        redirect(base_url()."be/san-pham/$id?err=Hình%20không%20đúng%20định%20dạng");
                    }
                    $config['overwrite']            =true;
					$config['upload_path']          = './image/product';
					$config['allowed_types']        = '*';
					$config['file_name']            =$id.".png";
                	$config['max_width']            = 2048;
                	$config['max_height']           = 1500;
                	$this->load->library('upload', $config);
                	if ( ! $this->upload->do_upload('hinh'))
                	{
                        $error = array('error' => $this->upload->display_errors());
                    }
        }
        $product=new product();
        $rt=$product->update($id,$_POST['name'],$_POST['des'],$_POST['price'],$_POST['type'],$_POST['status']);
        if($rt!='')
            {
                redirect(base_url()."be/that-bai?page=san-pham&err=$rt");
            }
            else
            {
                $log=new logs();
                $log->insert($_SESSION['user_data']['username'],"Đã cập nhật","sản phẩm",$id);
                 redirect(base_url()."be/thanh-cong?page=san-pham");
            }
    }
}
?>