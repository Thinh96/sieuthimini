<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Product_type_C extends CI_Controller {
	public function insert()
	{
        if(!isset($_POST['submit']) || !isset($_POST['name']) || !isset($_POST['parent']))
        {
            redirect(base_url()."be/them-danh-muc");
        }
        $type=new product_type();
        if($_POST['parent']==0) $pr='NULL';else $pr=$_POST['parent'];
        $rt=$type->insert($_POST['name'],$pr,1);
        if($rt!='')
            {
                redirect(base_url()."be/that-bai?page=danh-muc&err=$rt");
            }
            else
            {
                $log=new logs();
                $last=$type->getlast();
                $log->insert($_SESSION['user_data']['username'],"Đã thêm","Loại sản phẩm",$last[0]['id']);
                 redirect(base_url()."be/thanh-cong?page=danh-muc");
            }
    }
    public function delete($id=0)
	{
        if($id==0)
        {redirect(base_url()."be/quan-ly-danh-muc");}
        $type=new product_type();
        $rt=$type->delete($id);
        if($rt!='')
        {
            redirect(base_url()."be/that-bai?page=danh-muc&err=$rt");
        }
        else
        {
            $log=new logs();
            $log->insert($_SESSION['user_data']['username'],"Đã xóa","loại sản phẩm",$id);
            redirect(base_url()."be/thanh-cong?page=danh-muc");
        }
    }
    public function update($id=0)
	{
        if($id==0)
        {redirect(base_url()."be/quan-ly-danh-muc");}
        $type=new product_type();
        if($_POST['parent']==0) $pr="NULL"; else $pr=$_POST['parent'];
        $rt=$type->update($id,$_POST['name'],$pr,$_POST['status']);
        if($rt!='')
        {
            redirect(base_url()."be/that-bai?page=danh-muc&err=$rt");
        }
        else
        {
            $log=new logs();
            $log->insert($_SESSION['user_data']['username'],"Đã cập nhật","Loại sản phẩm",$id);
            redirect(base_url()."be/thanh-cong?page=danh-muc");
        }
    }
}
?>