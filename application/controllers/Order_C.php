<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Order_C extends CI_Controller {
	public function insert()
	{
        if(!isset($_POST['name']) || !isset($_POST['gender']) || !isset($_POST['birth'])|| !isset($_POST['id_card']) || !isset($_POST['phone']) || !isset($_POST['mail']) || !isset($_POST['type']))
        {
            redirect(base_url()."thanh-toan");
        }
        $display=new display();
        $pay_mail=$display->searchbyName('mail');
        $pay_mail=$pay_mail[0]['content'];
        $user=new user();
        $order=new order();
        $o_d=new order_detail();
        $rt=$user->insert($_POST['name'],$_POST['gender'],$_POST['birth'],$_POST['add'],$_POST['phone'],$_POST['mail'],$_POST['id_card'],1);
        if($rt=="")
        {
            $status=1;
            if($_POST['type']==2) $status=2;
            $last=$user->getlast();
            $user=$last[0]['id'];
            $p=new product();
            $pr=new promotion();
            $total=0;
            $rate=0;
            $vcrate=0;
            $or_last=0;
            foreach($_SESSION['user_data']['cart']['list'] as $k=>$v)
            {
                $p_=$p->getbyID($v['id']);
                $pr_=$pr->getallbyProduct($v['id']);
                if(count($pr_)>0) $rate=$pr_[0]['rate'];
                $total+=$v['amount']*$p_[0]['price']*(1-$rate/100);
            }
            $voucher=new voucher();$vc=$voucher->getbyId($_SESSION['user_data']['cart']['voucher']);
            if(count($vc)>0) $vcrate=$vc[0]['rate'];
            $total=$total*(1-$vcrate/100);
            if($_SESSION['user_data']['username']=="") $username='NULL';
            else $username=$_SESSION['user_data']['username'];
            $rt2=$order->insert($total,$user,$_SESSION['user_data']['cart']['total'],$_SESSION['user_data']['cart']['voucher'],$_POST['type'],$status,$username);
            if($rt2=="")
            {
                $or_last=$order->getlast();
                foreach($_SESSION['user_data']['cart']['list'] as $k=>$v)
                {
                    $o_d->insert($or_last[0]['id'],$v['id'],$v['amount']);
                }
                $_SESSION['user_data']['cart']['amount']=0;
                $_SESSION['user_data']['cart']['list']=array();
                $_SESSION['user_data']['cart']['voucher']=0;
                $_SESSION['user_data']['cart']['total']=0;
            }
            if($_POST['type']==2)
            {
            redirect("https://www.nganluong.vn/button_payment.php?receiver=".$pay_mail."&product_name=".$or_last[0]['id']."&price=".$total."&return_url=".base_url()."return%47".$or_last[0]['id']."&comments=");
            }
        }
        redirect(base_url());
    }
    public function delete($id=0)
	{
        if($id==0)
        {redirect(base_url()."be/quan-ly-hoa-don");}
        $news=new order();
        $rt=$news->delete($id);
        if($rt!='')
        {
            redirect(base_url()."be/that-bai?page=hoa-don&err=$rt");
        }
        else
        {
            $log=new logs();
            $log->insert($_SESSION['user_data']['username'],"Đã hủy","đơn hàng",$id);
            redirect(base_url()."be/thanh-cong?page=hoa-don");
        }
    }
    public function update($id=0)
	{
        if($id==0)
        {redirect(base_url()."be/quan-ly-hoa-don");}
        $news=new order();
        $rt=$news->update($id,$_POST['status']);
        if($rt!='')
        {
            redirect(base_url()."be/that-bai?page=hoa-don&id=$id&err=$rt");
        }
        else
        {
            $log=new logs();
            $log->insert($_SESSION['user_data']['username'],"Đã cập nhật","đơn hàng",$id);
            redirect(base_url()."be/thanh-cong?page=hoa-don&id=$id");
        }
    }
    public function rt($id=0)
    {
        if($id==0) ;
        else{
            if(isset($_GET['error_code']))
            if($_GET['error_code']!=""){
            $news=new order();
            $rt=$news->update($id,1);
            }
        }
        redirect(base_url());
    }
}
?>