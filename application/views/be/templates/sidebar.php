<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar" style='max-height:100vh;overflow:auto;position:fixed;'>
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar" >
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Menu chính</li>
        <li class='<?php if($page=='trang-chu') echo 'active ';?>'>
          <a href="<?php echo base_url();?>be">
           Trang chủ
          </a>
        </li>
        <?php if($_SESSION['user_data']['role']>3) {?>
        <li class="<?php if($page=='quan-ly-menu'||$page=='quan-ly-slider'||$page=='quan-ly-hinh-anh'||$page=='quan-ly-thong-tin-website') echo 'active';?> treeview">
          <a href="<?php echo base_url();?>be/giao-dien">
            <i class="fa fa-file-o"></i>
            <span>Giao diện</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url();?>be/quan-ly-menu"><i class="fa fa-circle-o"></i> Menu</a></li>
            <li><a href="<?php echo base_url();?>be/quan-ly-slider"><i class="fa fa-circle-o"></i> Slider</a></li>
            <li><a href="<?php echo base_url();?>be/quan-ly-hinh-anh"><i class="fa fa-circle-o"></i> Hình ảnh</a></li>
            <li><a href="<?php echo base_url();?>be/quan-ly-thong-tin-website"><i class="fa fa-circle-o"></i> Thông tin website</a></li>
          </ul>
        </li>
        <?php } ?>
        <li class='<?php if($page=='quan-ly-danh-muc') echo 'active ';?>'>
          <a href="<?php echo base_url();?>be/quan-ly-danh-muc">
            <i class="fa fa-th"></i> <span>Danh mục</span>
          </a>
        </li>
        <li class='<?php if($page=='quan-ly-san-pham') echo 'active ';?>'>
          <a href="<?php echo base_url();?>be/quan-ly-san-pham">
            <i class="fa fa-th"></i> <span>Sản phẩm</span>
          </a>
        </li>
        <li class='<?php if($page=='quan-ly-khuyen-mai') echo 'active ';?>'>
          <a href="<?php echo base_url();?>be/quan-ly-khuyen-mai">
            <i class="fa fa-tag"></i> <span>Khuyến mại</span>
          </a>
        </li>
        <li class='<?php if($page=='quan-ly-voucher') echo 'active ';?>'>
          <a href="<?php echo base_url();?>be/quan-ly-voucher">
            <i class="fa fa-tags"></i> <span>Voucher</span>
          </a>
        </li>
        <li class='<?php if($page=='quan-ly-tin-tuc') echo 'active ';?>'>
          <a href="<?php echo base_url();?>be/quan-ly-tin-tuc">
            <i class="fa fa-newspaper-o"></i> <span>Tin tức</span>
          </a>
        </li>
        <li class='<?php if($page=='quan-ly-khach-hang') echo 'active ';?>'>
          <a href="<?php echo base_url();?>be/quan-ly-khach-hang">
            <i class="fa fa-user-o"></i> <span>Khách hàng</span>
          </a>
        </li>
        <li class='<?php if($page=='quan-ly-hoa-don') echo 'active ';?>'>
          <a href="<?php echo base_url();?>be/quan-ly-hoa-don">
            <i class="fa fa-shopping-cart"></i> <span>Đơn hàng</span>
          </a>
        </li>
        <?php if($_SESSION['user_data']['role']>3) {?>
        <li class='<?php if($page=='quan-ly-nhan-vien') echo 'active ';?>'>
          <a href="<?php echo base_url();?>be/quan-ly-nhan-vien">
            <i class="fa fa-user-secret"></i> <span>Nhân viên</span>
          </a>
        </li>
        <?php } ?>
        <!-- <li class='<?php //if($page=='thong-ke') echo 'active ';?>'>
          <a href="<?php //echo base_url();?>be/thong-ke">
            <i class="fa fa-area-chart"></i> <span>Thống kê</span>
          </a>
        </li>-->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>