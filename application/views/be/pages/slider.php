<?php $slider=new slider();
$sd=$slider->getbyId($id);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $title;?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>be/"></i> Trang chủ</a></li>
      </ol>
    </section>
    <section class='content'>
    <div class="row">
        <!-- left column -->
        <div class='col-md-6'>
            <div class="box box-primary">
                <div class="box-header with-border">
                <h3 class="box-title">Thông tin</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" enctype="multipart/form-data" id='form1' method='post' action='<?php echo base_url();?>be/slider/update/<?php echo $id; ?>'>
                <div class="box-body">
                    <div class="form-group">
                    Tiêu đề:<input type="input" class="form-control" id="name" name='title' value='<?php echo $sd[0]['title'];?>' placeholder="Nhập tiêu đề slider">
                    Chữ nhỏ:<input type="input" class="form-control" id="name" name='small' value='<?php echo $sd[0]['small'];?>' placeholder="Nhập phần chữ nhỏ slider">
                 </div>
                <!-- /.box-body -->
                Vị trí:<select name='position' class='form-control'>
                    <?php 
                    for($i=1;$i<=count($slider->getall());$i++)
                    {
                      ?>
                      <option value='<?php echo $i;?>' <?php if($sd[0]['position']==$i) echo 'selected';?>><?php echo $i;?></option>
                      <?php
                    }
                    ?>
                    </select>
                <div class="box-footer">
                    <button type="submit" name='submit' class="btn btn-default">Cập nhật</button>
                </div>
                </form>
          </div>
        </div>
    </div>
    <div class="row">
        <!-- left column -->
        <div class='col-md-6'>
            <div class="box box-primary">
                <div class="box-header with-border">
                <h3 class="box-title">Hình ảnh</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <div class="form-group">
                    <img src="<?php echo base_url();?>image/slider_<?php echo $id;?>.png" width=300>
                    <br><input form='form1' type="file" class="form-control" id="hinh" name='hinh'>
                    <div style='width:0;height:0;overflow:hidden;'>
                    </div>
                </div>
          </div>
        </div>
    </div>
    </section>
  </div>
  <!-- /.content-wrapper -->