<?php 
$news=new news();
$lst=$news->getall();
?><!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $title;?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>be/"></i> Trang chủ</a></li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            <div class="box-footer clearfix no-border">
              <a href='<?php echo base_url();?>be/them-tin-tuc'><button type="button" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Thêm tin tức</button></a>
            </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Title</th>
                  <th>Người đăng</th>
                  <th>Trạng thái</th>
                  <th>Thao tác</th>
                </tr>
                </thead>
                <tbody>
                  <?php foreach($lst as $k=>$v) {
                    ?>
                    <tr>
                      <td><?php echo $v['id'];?></td>
                      <td><?php echo $v['title'];?></td>
                      <td><?php echo $v['username'];?></td>
                      <td><?php if($v['status']==1) echo "Đang hoạt động"; else echo "Ngừng hoạt động";?></td>
                      <td><a href='<?php echo base_url();?>be/tin-tuc/<?php echo $v['id']?>'><button title='Chi tiết'><i class='fa fa-edit'></i></button></a>&nbsp<a href='<?php echo base_url();?>be/tin-tuc/delete/<?php echo $v['id']?>'><button title='Xóa'><i class='fa fa-close'></i></button></a></td>
                    </tr>
                    <?php
                  }?>                
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  </div>
  <!-- /.content-wrapper -->