<?php if($_SESSION['user_data']['role']<4)
redirect(base_url()."be");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $title;?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>be/"></i> Trang chủ</a></li>
      </ol>
    </section>

 <section class='content'>
    <div class="row-auto">
        <!-- left column -->
        <div class='col-md-auto'>
            <div class="box box-primary">
                <div class="box-header with-border">
                <h3 class="box-title"></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" enctype="multipart/form-data" id='form1' method='post' action='<?php echo base_url();?>be/khuyen-mai/insert'>
                <div class="box-body">
                    <div class="form-group">
                    Mã sản phẩm:<input type="text" class="form-control" id="id" name='id' <?php if($id!=0) echo "value='".$id."'";?> placeholder="Nhập mã sản phẩm" required>
                    Tỉ lệ:<input type="number" class="form-control" id="rate" name='rate' placeholder="Nhập % khuyến mại" min=1 max=99 required>
                    Số lượng khuyến mại:<input type="num" min=0 class="form-control" id="name" name='amount' placeholder="Nhập số lượng khuyến mại" required>
                    Ngày bắt đầu:<input style='width:200px;' type="date" class="form-control" id="from" name='from' required>
                    Ngày kết thúc:<input style='width:200px;' type="date" class="form-control" id="to" name='to' required>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" name='submit' class="btn btn-default">Thêm</button>
                </div>
                </form>
            </div>
        </div>
        </div>
    </section>
  </div>
  <!-- /.content-wrapper -->
  <script>
  <?php if(isset($_GET['err']))
    ?>alert('<?php echo $_GET['err'];?>');<?php
  ?>
  
  </script>