<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $title;?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>be/"></i> Trang chủ</a></li>
      </ol>
    </section>
    <section class='content'>
    <div class="row-auto">
        <!-- left column -->
        <div class='col-md-auto'>
            <div class="box box-primary">
                <div class="box-header with-border">
                <h3 class="box-title"></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" enctype="multipart/form-data" id='form1' method='post' action='<?php echo base_url();?>be/tin-tuc/insert'>
                <div class="box-body">
                    <div class="form-group">
                    Tiêu đề:<input type="text" class="form-control" id="title" name='title' required>
                    Người đăng:<input type="text" value='<?php echo $_SESSION['user_data']['username'];?>' class="form-control" id="price" name='username' readonly>
                    Nội dung:
                  <div class="box-body pad">
                      <textarea name='content' class="textarea" placeholder="Place some text here"
                                style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                  </div>
                </div>
                      <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" name='submit' class="btn btn-default">Thêm</button>
                </div>
                </form>
            </div>
        </div>
        </div>
    </section>
  </div>
  <!-- /.content-wrapper -->
  <script>
  <?php if(isset($_GET['err']))
    ?>alert('<?php echo $_GET['err'];?>');<?php
  ?>
  
  </script>