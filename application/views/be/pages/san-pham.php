<?php 
$product=new product();
$p=$product->getbyId($id);
?><!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $title;?> : <?php echo $p[0]['name'];?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>be/"></i> Trang chủ</a></li>
      </ol>
    </section>
    <section class='content'>
    <div class="row">
        <!-- left column -->
        <div class='col-md-6'>
            <div class="box box-primary">
                <div class="box-header with-border">
                <h3 class="box-title">Thông tin</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" enctype="multipart/form-data" id='form1' method='post' action='<?php echo base_url();?>be/san-pham/update/<?php echo $id;?>'>
                <div class="box-body">
                    <div class="form-group">
                    Tên sản phẩm :<input type="input" class="form-control" id="name" name='name' value='<?php echo $p[0]['name'];?>' placeholder="Nhập tên sản phẩm">
                    Giá bán<input type="input" class="form-control" id="price" name='price'  value='<?php echo $p[0]['price'];?>' placeholder="Nhập giá" pattern='^(0|[1-9][0-9]*)$' title='Chỉ nhập số'>
                    Mô tả sản phẩm :<br><textarea name='des' form='form1' rows=5 class='form-control'><?php echo $p[0]['description'];?></textarea><br>
                    Loại sản phẩm(Danh mục):
                    <select class='form-control' name='type'>
                    <?php
                    $type=new product_type();
                    $pr=$type->getallParent();
                    foreach($pr as $k=>$v)
                    {
                      ?>
                      <option class='text-danger' disabled><?php echo $v['name'];?></option>
                      <?php
                      foreach($type->getallbyParent($v['id']) as $k1=>$v1)
                      {
                        ?>
                        <option value='<?php echo $v1['id'];?>' <?php if($v1['id'] == $p[0]['product_type']) echo 'selected'; ?>>&nbsp&nbsp<?php echo $v1['name'];?></option>
                        <?php
                      }
                    }
                    ?>
                    </select>
                    Trạng thái:
                    <select class='form-control' name='status'>
                    <option value=2 <?php if($p[0]['status']==2) echo 'selected';?>>Ngừng hoạt động</option>
                    <option value=1 <?php if($p[0]['status']==1) echo 'selected';?>>Hoạt động</option>
                    </select>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" name='submit' class="btn btn-default">Cập nhật</button>
                </div>
                </form>
            </div>
        </div>
        </div>
        <div class="row">
        <!-- left column -->
        <div class='col-md-6'>
            <div class="box box-primary">
                <div class="box-header with-border">
                <h3 class="box-title">Hình ảnh</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" enctype="multipart/form-data" id='form1' method='post' action='<?php echo base_url();?>be/san-pham/update/<?php echo $id;?>'>
                <div class="box-body">
                    <div class="form-group">
                    <img style='margin: 5px auto;' src='<?php echo base_url();?>image/product/<?php echo $id;?>' width=200>
                    <input form='form1' type="file" class="form-control" id="hinh" name='hinh' title='chọn hình'>
                    
                </div>
                <!-- /.box-body -->
                </form>
            </div>
        </div>
        </div>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
          <div class="box-footer clearfix no-border">
              <a href='<?php echo base_url();?>be/them-chi-tiet-san-pham?id=<?php echo $id;?>'><button type="button" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Thêm</button></a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Số lượng</th>
                  <th>Ngày nhập</th>
                  <th>Ngày sản xuất</th>
                  <th>Ngày hết hạn</th>
                  <th>Bảo hành</th>
                  <th>Thao tác</th>
                </tr>
                </thead>
                <tbody>
                  <?php $product_detail=new product_detail(); 
                  $lst=$product_detail->getbyProduct($id);
                  foreach($lst as $k=>$v) {
                    ?>
                    <tr class='<?php if($v['exp']!="0000-00-00 00:00:00" && $v['exp'] < date("Y-m-d H:m:s")) echo 'text-danger';?>'>
                      <td><?php echo $k+1;?></td>
                      <td><?php echo $v['amount'];?></td>
                      <td><?php echo date( "m/d/Y H:m:s", strtotime($v['created_at']));?></td>
                      <td><?php if($v['mfg']!="0000-00-00 00:00:00") {echo date( "m/d/Y", strtotime($v['mfg']));}?></td>
                      <td><?php if($v['exp']!="0000-00-00 00:00:00") {echo date( "m/d/Y", strtotime($v['exp']));}?></td>
                      <td><?php if($v['warr']!=0) echo $v['warr'];?></td>
                      <td><a href='<?php echo base_url();?>be/chi-tiet-san-pham/<?php echo $v['id']?>'><button><i class='fa fa-edit'></i></button></a>&nbsp<a href='<?php echo base_url();?>be/chi-tiet-san-pham/delete/<?php echo $v['id']?>'><button><i class='fa fa-close'></i></button></a></td>
                    </tr>
                    <?php
                  }?>                
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  </div>
  <!-- /.content-wrapper -->