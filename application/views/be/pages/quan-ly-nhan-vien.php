<?php 
$user=new user();
$lst=$user->getallbyRole(3);
if($_SESSION['user_data']['role']==5)
{
  $lst=$user->getallbyRole(4);
}
?><!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $title;?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>be/"></i> Trang chủ</a></li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            <div class="box-footer clearfix no-border">
              <a href='<?php echo base_url();?>be/them-nhan-vien'><button type="button" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Thêm nhân viên</button></a>
            </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Mã</th>
                  <th>Tên</th>
                  <th>Giới tính</th>
                  <th>Tài khoản</th>
                  <th>Thao tác</th>
                </tr>
                </thead>
                <tbody>
                  <?php $i=1; foreach($lst as $k=>$v) {
                    ?>
                    <tr>
                      <td><?php echo $v['id'];?></td>
                      <td><?php echo $v['name'];?></td>
                      <td><?php if($v['gender']==1) echo "Nam"; else echo "Nữ";?></td>
                      <td><?php echo $v['username'];?></td>
                      </td>
                      <td><a href='<?php echo base_url();?>be/khach-hang/<?php echo $v['id']?>'><button title='Chi tiết'><i class='fa fa-edit'></i></button></a></td>
                    </tr>
                    <?php
                    $i++;
                  }?>                
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  </div>
  <!-- /.content-wrapper -->