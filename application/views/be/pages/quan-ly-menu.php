<?php $menu=new menu(); 
$lst=$menu->getall();
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $title;?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>be/"></i> Trang chủ</a></li>
      </ol>
    </section>
  <section 'container'>
   <!-- TO DO List -->
       <div class="box box-primary col-md-4">
            <div class="box-header">
              <i class="ion ion-clipboard"></i>

              <h3 class="box-title">Danh mục:</h3>

            </div>
            
          <div class="box-body">
              <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
              <ul class="todo-list">
              <?php foreach($lst as $k=>$v)
              {
                  ?>
                  <li>
                  <!-- drag handle -->
                  <span class="handle">
                  <?php echo $k+1;?>
                  </span>
                  <!-- checkbox -->
                  <!-- todo text -->
                  <span class="text"><?php echo $v['name'];?></span>
                  <!-- Emphasis label -->
                  <!-- General tools such as edit or delete-->
                  <div class="tools">
                    <a href='<?php echo base_url();?>be/menu/<?php echo $v['id'];?>'><i class="fa fa-edit"></i></a>
                    <a href='<?php echo base_url();?>be/menu/delete/<?php echo $v['id'];?>'><i class="fa fa-trash-o"></i></a>
                  </div>
                </li>
                  <?php
              } ?>
                
              </ul>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix no-border">
              <a href='<?php echo base_url();?>be/them-menu'><button type="button" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Thêm menu</button></a>
            </div>
          </div>
  </section>
  </div>
  <!-- /.content-wrapper -->