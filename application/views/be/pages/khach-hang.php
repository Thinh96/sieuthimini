<?php 
$user=new user();
$u=$user->getbyId($id);
$role=$_SESSION['user_data']['role'];
?><!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $title;?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>be/"></i> Trang chủ</a></li>
        <li><a href="<?php echo base_url();?>be/them-menu"></i> Thêm menu</a></li>
      </ol>
    </section>
    <section class='content'>
    <div class="row-auto">
        <!-- left column -->
        <div class='col-md-auto'>   
            <div class="box box-primary">
                <div class="box-header with-border">
                <h3 class="box-title"></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                
                <div class="box-body">
                    <div class="form-group">
                    <label for="name1">Tên:</label>
                    <input type="input" class="form-control" id="name" name='name' value="<?php echo $u[0]['name'];?>" readonly>
                    </div>
                    <div class="form-group">
                    <label for="name1">Giới tính:</label>
                    <input type="input" class="form-control" id="name" name='name' value="<?php if($u[0]['name']) echo "Nam"; else echo "Nữ";;?>" readonly>
                    </div>
                    <div class="form-group">
                    <label for="name1">Địa chỉ:</label>
                    <input type="input" class="form-control" id="name" name='name' value="<?php echo $u[0]['address'];?>" readonly>
                    </div>
                    <div class="form-group">
                    <label for="name1">Số chứng minh:</label>
                    <input type="input" class="form-control" id="name" name='name' value="<?php echo $u[0]['id_card'];?>" readonly>
                    </div>
                    <div class="form-group">
                    <label for="name1">Điện thoại:</label>
                    <input type="input" class="form-control" id="name" name='name' value="<?php echo $u[0]['phone'];?>" readonly>
                    </div>
                    <div class="form-group">
                    <label for="name1">Mail:</label>
                    <input type="input" class="form-control" id="name" name='name' value="<?php echo $u[0]['email'];?>" readonly>
                    </div>
                    <form role="form" method='post'  action='<?php echo base_url();?>be/user/<?php echo $id;?>' enctype="multipart/form-data" >
                    <div class="form-group">
                    <label for="name1">Chức vụ:</label>
                    <select class='form-control' style='width:200px' name='role'>
                    <option value=2 <?php if($u[0]['role']==2) echo 'selected';?>>Thành viên</option>
                    <option value=3 <?php if($u[0]['role']==3) echo 'selected';?>>Nhân viên</option>
                    <?php if($role==5) {?><option value=4 <?php if($u[0]['role']==4) echo 'selected';?>>Quản lý</option><?php }?>
                    </select><br>
                    <input type='submit' value='Cập nhật' class='btn btn-default'>
                    </div>
                    </form>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                </div>
                
            </div>
        </div>
        </div>
    </section>
  </div>
  <!-- /.content-wrapper -->