<?php $role= $_SESSION['user_data']['role'];?><!-- Content Wrapper. Contains page content -->
<?php $type=new product_type();?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $title;?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>be/"></i> Trang chủ</a></li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
          <div class="box-footer clearfix no-border">
<?php if($role>3){?><a href='<?php echo base_url();?>be/them-danh-muc'><button type="button" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Thêm danh mục</button></a><?php }?>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <small class='text-danger'>*Chỉ có thể xóa danh mục rỗng</small>
                  <?php $lst=$type->getallParent(); foreach($lst as $k=>$v) {
                    ?>
                    
                    <table class="table table-bordered table-striped">
                    <tr><td style='fon-size:20px;' data-toggle="collapse" data-target="#t<?php echo $k;?>"><?php echo $v['name'];?><?php 
                    if($v['status']==0)
                    {
                        ?>
                        <small class='text-danger'>(Ngừng hoạt động)</small>
                        <?php
                    }
                    ?></td>
                    <?php if($role>3){?><td style='width:100px;'><a href='<?php echo base_url();?>be/danh-muc/<?php echo $v['id']?>'><button><i class='fa fa-edit'></i><?php }?>
                    <?php if(count($type->getallbyParent($v['id']))==0)
                    {
                        ?>
                        <?php if($role>3){?></button></a>&nbsp<a href='<?php echo base_url();?>be/danh-muc/delete/<?php echo $v['id']?>'><button><i class='fa fa-close'></i></button></a><?php } ?>
                        <?php
                    }
                    ?></td>
                    </tr></table>
                    <div id="t<?php echo $k;?>" class="collapse">
                    <table id="example1" class="table table-bordered table-striped">
                    <?php $ds=$type->getallbyParent($v['id']);
                    foreach($ds as $k1=> $v1){
                      $p=new product();
                      $lst=$p->getbyType($v1['id']);
                        ?>
                        <tr>
                      <td style='width:50px;'><?php echo $k1+1;?></td>
                      <td><?php echo $v1['name'];?> <?php if($v1['status']==0)
                      
                    {
                        ?>
                        <small class='text-danger'>(Ngừng hoạt động)</small>
                        <?php
                    }
                    ?></td>
                      <?php if($role>3){?><td  style='width:100px;'><a href='<?php echo base_url();?>be/danh-muc/<?php echo $v1['id']?>'><button><i class='fa fa-edit'></i></button></a>&nbsp<?php if(count($lst)==0){?><a href='<?php echo base_url();?>be/danh-muc/delete/<?php echo $v1['id']?>'><button><i class='fa fa-close'></i></button></a></td><?php } } ?>
                    </tr>
                        <?php
                    }
                    ?>
                    </table>
                    </div>
                    <?php
                  }?>                
              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>  
  </div>
  <!-- /.content-wrapper -->