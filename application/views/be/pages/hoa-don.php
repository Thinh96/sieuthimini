<?php $voucher=new voucher();$vc=$voucher->getbyId($_SESSION['user_data']['cart']['voucher']);
	if($vc==array())
	{
		$vc[0]=array('rate'=>0);
	}
	?><!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Đơn hàng
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>be/"></i> Trang chủ</a></li>
      </ol>
    </section>
<?php 
$order=new order();
$or=$order->searchbyId($id);
$or_d=new order_detail();
$ord_d=$or_d->searchbyOrder($id);
$user=new user();
$u=$user->getbyId($or[0]['user']);
$p=new product();
?>
 <section class='content'>
 <div class='row'>
	<div class='col-md-12'>
	<div class='box'>
	<h3>THÔNG TIN NGƯỜI NHẬN:</h3>
	<big>
	<table>
	<tr><td>Họ tên:</td><td style='font-weight:bold'><?php echo $u[0]['name'];?></td></tr>
	<tr><td>Giới tính:</td><td style='font-weight:bold'><?php if($u[0]['gender']==1) echo "Nam"; else echo "Nữ";?></td></tr>
	<tr><td>Ngày sinh:</td><td style='font-weight:bold'><?php echo date("d/m/Y",strtotime($u[0]['birthday']));?></td></tr>
	<tr><td>Địa chỉ:</td><td style='font-weight:bold'><?php echo $u[0]['address'];?></td></tr>
	<tr><td>Số chứng minh:</td><td style='font-weight:bold'><?php echo $u[0]['id_card'];?></td></tr>
	<tr><td>Mail:</td><td style='font-weight:bold'><?php echo $u[0]['email'];?></td></tr>
	<tr><td>Số điện thoại:</td><td style='font-weight:bold'><?php echo $u[0]['phone'];?></td></tr>
	</table></big>
	<h3>THÔNG TIN ĐƠN HÀNG:</h3>
	<h4>Loại đơn hàng:<?php if($or[0]['type']==1) echo "Trả sau"; else echo "Trả trước";?></h4>
	<big>
	<table border=1 style='width:100%'>
	<tr><th>Tên sản phẩm</th><th>Số lượng</th><th>Giá</th></tr>
	<?php 
	foreach($ord_d as $k=>$v)
	{
		$pd=$p->getbyId(($v['product']));
		?>
		<tr>
		<td><?php echo $pd[0]['name']; ?></td>
		<td><?php echo $v['amount']; ?></td>
		<td><?php echo number_format($pd[0]['price']); ?></td>
		</tr>
		<?php
	}
	?>
	<tr><th colspan=2 style='text-align:right'>Tổng </th><th> <?php echo number_format($or[0]['total']);?></th></tr>
	</table>
	</big>
	<br>
	<form action='<?php echo base_url();?>order/update/<?php echo $id;?>' method='post'>
	Trạng thái:<select name='status' class='inline form-control' style='width:200px'>
	<option value=0 <?php if($or[0]['status']==0) echo "selected";?>>Hủy</option>
	<option value=1 <?php if($or[0]['status']==1) echo "selected";?>>Xử lý</option>
<?php if($or[0]['type']==2) {?><option value=2 <?php if($or[0]['status']==2) echo "selected";?>>Chưa thanh toán</option><?php } ?>
	<option value=3 <?php if($or[0]['status']==3) echo "selected";?>>Vận chuyển</option>
	</select>
	<input type='submit' value='Cập nhật'>
	</form>
	</div></div>
 </div>
 </section>
  </div>
  <!-- /.content-wrapper -->
  <script>
  <?php if(isset($_GET['err']))
    ?>alert('<?php echo $_GET['err'];?>');<?php
  ?>
  </script>