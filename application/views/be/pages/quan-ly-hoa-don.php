<?php 
$order=new order();
$lst=$order->getall();
?><!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $title;?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>be/"></i> Trang chủ</a></li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            <div class="box-footer clearfix no-border">
              
            </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Mã hóa đơn</th>
                  <th>Mã khách hàng</th>
                  <th>Tổng tiền</th>
                  <th>Loại</th>
                  <th>Trạng thái</th>
                  <th>Thao tác</th>
                </tr>
                </thead>
                <tbody>
                  <?php $i=1; foreach($lst as $k=>$v) {
                    ?>
                    <tr>
                      <td><?php echo $v['id'];?></td>
                      <td><?php echo $v['user'];?></td>
                      <td><?php echo number_format($v['total']);?></td>
                      <td><?php if($v['type']==1) echo "Trả sau"; else echo "Trả trước";?></td>
                      <td><?php switch($v['status'])
                      {
                        case 0: echo "Hủy"; break;
                        case 1: echo "Xử lý";break;
                        case 2: echo "Chưa thanh toán";break;
                        case 3: echo "Vận chuyển";break;
                      }?></td>
                      </td>
                      <td><a href='<?php echo base_url();?>be/hoa-don/<?php echo $v['id']?>'><button title='Chi tiết'><i class='fa fa-edit'></i></button></a></td>
                    </tr>
                    <?php
                    $i++;
                  }?>                
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  </div>
  <!-- /.content-wrapper -->