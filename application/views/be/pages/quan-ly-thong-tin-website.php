<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $title;?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>be/"></i> Trang chủ</a></li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
           <?php $display=new display();?>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Thông tin website</th><th></th><th></th>
                </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Mail:</td><td><form action='<?php echo base_url();?>be/thong-tin/mail' method='post'><input type='text' name='content' class='form-control' value='<?php echo $display->searchbyName('mail')[0]['content'];?>' required><input type='submit' value='Cập nhật'></form></td>
                  </tr>
                  <tr>
                  <td>Map:</td><td><form action='<?php echo base_url();?>be/thong-tin/map' method='post'><input type='text' name='content' class='form-control' value='<?php echo $display->searchbyName('map')[0]['content'];?>' required><input type='submit' value='Cập nhật'></form></td>
                  </tr>
                  <tr>
                  <td>Facebook:</td><td><form action='<?php echo base_url();?>be/thong-tin/fb' method='post'><input type='text' name='content' class='form-control' value='<?php echo $display->searchbyName('fb')[0]['content'];?>' required><input type='submit' value='Cập nhật'></form></td>
                  </tr>
                  <tr>
                  <td>Tiêu đề nhỏ(hình giữa trang chủ):</td><td><form action='<?php echo base_url();?>be/thong-tin/middlesmall' method='post'><input type='text' name='content' class='form-control' value='<?php echo $display->searchbyName('middle small')[0]['content'];?>' required><input type='submit' value='Cập nhật'></form></td>
                  </tr>
                  <tr>
                  <td>Tiêu đề lớn(hình giữa trang chủ):</td><td><form action='<?php echo base_url();?>be/thong-tin/middletitle' method='post'><input type='text' name='content' class='form-control' value='<?php echo $display->searchbyName('middle title')[0]['content'];?>' required><input type='submit' value='Cập nhật'></form></td>
                  </tr>
                  <tr>
                  <td>Mô tả:</td><td><form action='<?php echo base_url();?>be/thong-tin/des' method='post'><input type='text' name='content' class='form-control' value='<?php echo $display->searchbyName('Mô tả')[0]['content'];?>' required><input type='submit' value='Cập nhật'></form></td>
                  </tr>
                  <tr>
                  <td>Địa chỉ:</td><td><form action='<?php echo base_url();?>be/thong-tin/add' method='post'><input type='text' name='content' class='form-control' value='<?php echo $display->searchbyName('Địa chỉ')[0]['content'];?>' required><input type='submit' value='Cập nhật'></form></td>
                  </tr>
                  <tr>
                  <td>Số điện thoại:</td><td><form action='<?php echo base_url();?>be/thong-tin/phone' method='post'><input type='text' name='content' class='form-control' value='<?php echo $display->searchbyName('Điện thoại')[0]['content'];?>' required><input type='submit' value='Cập nhật'></form></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  </div>
  <!-- /.content-wrapper -->