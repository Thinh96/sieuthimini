<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Trag chủ 
        <small>Trang quản lý</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>be"><i class="fa fa-dashboard"></i>Trang chủ</a></li>
      </ol>
    </section>
<!-- Main content -->
<section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php
              $or=new order();
              echo count($or->getall());
              ?></h3>

              <p>Đơn hàng</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="<?php echo base_url();?>be/quan-ly-hoa-don" class="small-box-footer">Xem thêm <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php 
              $user=new user();
              echo count($user->getall());
              ?></h3>

              <p>Thành viên</p>
            </div>
            <div class="icon">
              <i class="fa fa-user"></i>
            </div>
            <a href="<?php echo base_url();?>be/quan-ly-khach-hang" class="small-box-footer">Xem thêm <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php 
              $display=new display();
              $view=$display->searchbyName('daily view');
              echo $view[0]['content'];
              ?></h3>

              <p>Lượt xem trong ngày</p>
            </div>
            <div class="icon">
              <i class="fa fa-eye"></i>
            </div>
            <a class="small-box-footer">&nbsp</a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
            <h3><?php 
              $display=new display();
              $view=$display->searchbyName('total view');
              echo $view[0]['content'];
              ?></h3>

              <p>Tổng lượt xem</p>
            </div>
            <div class="icon">
              <i class="fa fa-eye"></i>
            </div>
            <a class="small-box-footer">&nbsp</a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <div class='col-md-8'>
          <div class="box">
            <div class="box-header">
            Hoạt động:
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>User</th>
                  <th>Hoạt động</th>
                  <th>Loại</th>
                  <th>Mã</th>
                  <th>Thời gian</th>
                </tr>
                </thead>
                <tbody>
                  <?php $log=new logs(); $lst=$log->getall(); $i=1; foreach($lst as $k=>$v) {
                    ?>
                    <tr>
                    <td><?php echo $v['user']; ?></td>
                    <td><?php echo $v['action']; ?></td>
                    <td><?php echo $v['type']; ?></td>
                    <td><?php echo $v['id_type']; ?></td>
                    <td><?php echo date("d/m/Y H:i:s",strtotime($v['created_at'])); ?></td>
                    </tr>
                    <?php
                  }?>                
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <div class='col-md-4'>
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">
              <?php $p=new product();
              $p_d=new product_detail();
              $n_p=0;
              foreach($p->getall() as $k=>$v)
              {
                if(count($p_d->getbyProduct($v['id']))==0) $n_p++;
              }
              echo $n_p;
              ?> Sản phẩm hết hàng
              </span>
              <span class="info-box-number">
               <a style='color:white' href="<?php echo base_url();?>be/quan-ly-san-pham" class="small-box-footer">XEM <i class="fa fa-arrow-circle-right"></i></a>
              </span>

              <div class="progress">
                <div class="progress-bar" style="width: <?php echo $n_p/count($p->getall())*100; ?>%"></div>
              </div>
              <span class="progress-description">
              TỔNG: <?php echo count($p->getall()); ?> Sản phẩm
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Khuyến mại</span>
              <span class="info-box-number">
               <a style='color:white' href="<?php echo base_url();?>be/quan-ly-khuyen-mai" class="small-box-footer">XEM <i class="fa fa-arrow-circle-right"></i></a>
              </span>

              <div class="progress">
                <div class="progress-bar" style="width: 0%"></div>
              </div>
              <span class="progress-description">
                    <?php $pr=new promotion();
                    echo count($pr->getall());
                    ?> Khuyến mại đang diễn ra
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Voucher</span>
              <span class="info-box-number">
               <a style='color:white' href="<?php echo base_url();?>be/quan-ly-voucher" class="small-box-footer">XEM <i class="fa fa-arrow-circle-right"></i></a>
              </span>

              <div class="progress">
                <div class="progress-bar" style="width: 0%"></div>
              </div>
              <span class="progress-description">
              <?php $pr=new voucher();
                    echo count($pr->getall());
                    ?> Voucher đang hoạt động
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
          <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="ion-ios-chatbubble-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Hỗ trợ khách hàng(Message)</span>
              <span class="info-box-number">
               <a style='color:white' href="https://dashboard.tawk.to" class="small-box-footer">Xem <i class="fa fa-arrow-circle-right"></i></a>
              </span>

              <div class="progress">
                <div class="progress-bar" style="width: 0%"></div>
              </div>
              <span class="progress-description">
                    Tawk.io
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
        </div>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->