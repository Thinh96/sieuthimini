<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $title;?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>be/"></i> Trang chủ</a></li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
           
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Tiêu đề</th>
                  <th>Ảnh</th>
                  <th width=50>Kích thước</th>
                  <th width=100>Thao tác</th>
                </tr>
                <tr><td>Icon website</td><td><img src='<?php echo base_url();?>bootstrap/fe/images/icons/favicon.png'></td>
                <td>11x11</td>
                <td><form method='post' action='<?php echo base_url();?>be/quan-ly-hinh-anh/icon' enctype="multipart/form-data"><input type='file' name='hinh' required><input type='submit' value='Thay'></form></td>
                </tr>

                <tr><td>Logo website</td><td><img src='<?php echo base_url();?>bootstrap/fe/images/icons/logo.png'></td>
                <td>120x27</td>
                <td><form  method='post'  action='<?php echo base_url();?>be/quan-ly-hinh-anh/favicon' enctype="multipart/form-data"><input type='file' name='hinh' required><input type='submit' value='Thay'></form></td>
                </tr>

                <tr><td>Hình giữa trang chủ</td><td><img src='<?php echo base_url();?>image/homemiddle.png' width=200></td>
                <td>800x377</td>
                <td><form  method='post'  action='<?php echo base_url();?>be/quan-ly-hinh-anh/homemiddle' enctype="multipart/form-data"><input type='file' name='hinh' required><input type='submit' value='Thay'></form></td>
                </tr>

                <tr><td>Hình tiêu đề trang</td><td><img src='<?php echo base_url();?>image/title.png' width=200></td>
                <td>1930x239</td>
                <td><form  method='post'  action='<?php echo base_url();?>be/quan-ly-hinh-anh/title' enctype="multipart/form-data"><input type='file' name='hinh' required><input type='submit' value='Thay'></form></td>
                </tr>

                <?php $type=new product_type();
                foreach($type->getallParent() as $k=>$v)
                {
                  ?>
                  <tr><td><?php echo $v['name'];?></td><td><img src='<?php echo base_url();?>image/type_<?php echo $v['id']; ?>.png' width=200></td>
                  <td>720x660</td>
                  <td><form action='<?php echo base_url();?>be/quan-ly-hinh-anh/type_<?php echo $v['id'];?>' enctype="multipart/form-data" method='post'><input type='file' name='hinh' required><input type='submit' value='Thay'></form></td>
                  </tr>
                  <?php
                }
                $new=new news();
                foreach($new->getall() as $k=>$v)
                {
                  ?>
                  <tr><td>Tin tức:<?php echo $v['id'];?></td><td><img src='<?php echo base_url();?>image/tin-tuc-<?php echo $v['id']; ?>.png' width=200></td>
                  <td>720x660</td>
                  <td><form action='<?php echo base_url();?>be/quan-ly-hinh-anh/tin-tuc-<?php echo $v['id'];?>' method='post' enctype="multipart/form-data"><input type='file' name='hinh' required><input type='submit' value='Thay'></form></td>
                  </tr>
                  <?php
                }
                ?>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  </div>
  <!-- /.content-wrapper -->