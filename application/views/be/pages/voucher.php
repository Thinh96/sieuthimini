<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $title;?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>be/"></i> Trang chủ</a></li>
      </ol>
    </section>
<?php 
$voucher=new voucher();
$vc=$voucher->getbyId($id);
$user=new user();
$u=$user->getbyId($vc[0]['user']);
?>
 <section class='content'>
    <div class="row-auto">
        <!-- left column -->
        <div class='col-md-auto'>
            <div class="box box-primary">
                <div class="box-header with-border">
                <h3 class="box-title"></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" enctype="multipart/form-data" id='form1' method='post' action='<?php echo base_url();?>be/voucher/update/<?php echo $id;?>'>
                <div class="box-body">
                    <div class="form-group">
                    Người dùng:<input type="text" class="form-control" id="user" name='user' value='<?php echo $vc[0]['user'];?>' placeholder="Mã người dùng" min=1 max=99 readonly>
                    Tỉ lệ:<input type="number" class="form-control" id="rate" name='rate' value='<?php echo $vc[0]['rate'];?>' placeholder="Nhập % khuyến mại" min=1 max=99 required>
                    Số lần dùng:<input type="number" min=0 class="form-control" id="name"  value='<?php echo $vc[0]['amount'];?>' name='amount' placeholder="Nhập số lượng khuyến mại" required>
                    Ngày bắt đầu:<input style='width:200px;' type="date" class="form-control" id="from" name='from'  value='<?php $originalDate = $vc[0]['from_date']; echo $newDate = date("Y-m-d", strtotime($originalDate));?>' required>
                    Ngày kết thúc:<input style='width:200px;' type="date" class="form-control" id="to" name='to'  value='<?php $originalDate = $vc[0]['to_date']; echo $newDate = date("Y-m-d", strtotime($originalDate));?>' required>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" name='submit' class="btn btn-default">Cập nhật</button>
                </div>
                </form>
            </div>
        </div>
        </div>
    </section>
  </div>
  <!-- /.content-wrapper -->
  <script>
  <?php if(isset($_GET['err']))
    ?>alert('<?php echo $_GET['err'];?>');<?php
  ?>
  </script>