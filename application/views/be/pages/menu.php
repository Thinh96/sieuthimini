<?php $menu=new menu();
$mn=$menu->getbyId($id);
if(count($mn)==0)
show_404();
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $title;?> - <?php echo $mn[0]['name'];?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>be/"></i> Trang chủ</a></li>
      </ol>
    </section>
    <section class='content'>
    <div class="row">
        <!-- left column -->
        <div class='col-md-6'>
            <div class="box box-primary">
                <div class="box-header with-border">
                <h3 class="box-title"></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" enctype="multipart/form-data" id='form1' method='post' action='<?php echo base_url();?>be/menu/update/<?php echo $id; ?>'>
                <div class="box-body">
                    <div class="form-group">
                    <label for="name1">Tên:</label>
                    <input type="input" class="form-control" id="name" name='name' value='<?php echo $mn[0]['name'];?>' placeholder="Nhập tên menu">
                    Vị trí:<select name='position' class='form-control'>
                    <?php 
                    for($i=1;$i<=count($menu->getall());$i++)
                    {
                      ?>
                      <option value='<?php echo $i;?>' <?php if($mn[0]['position']==$i) echo 'selected';?>><?php echo $i;?></option>
                      <?php
                    }
                    ?>
                    </select>
                    <div style='width:0;height:0;overflow:hidden;'>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" name='submit' class="btn btn-default">Cập nhật</button>
                </div>
                </form>
            </div>
        </div>
        </div>
    </section>
  </div>
  <!-- /.content-wrapper -->