<?php
  $news=new news();
  $ns=$news->getbyId($id);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $title;?>:<?php echo $ns[0]['title']; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>be/"></i> Trang chủ</a></li>
      </ol>
    </section>
    <section class='content'>
    <div class="row-auto">
        <!-- left column -->
        <div class='col-md-auto'>
            <div class="box box-primary">
                <div class="box-header with-border">
                <h3 class="box-title"></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" enctype="multipart/form-data" id='form1' method='post' action='<?php echo base_url();?>be/tin-tuc/update/<?php echo $id;?>'>
                <div class="box-body">
                    <div class="form-group">
                    Tiêu đề:<input type="text" class="form-control" id="title" name='title' value='<?php echo $ns[0]['title']; ?>' required>
                    Trạng thái:<select name='status' class='form-control'>
                    <option value=1 <?php if($ns[0]['status']==1) echo"selected";?>>Đang hoạt động</option>
                    <option value=0 <?php if($ns[0]['status']==0) echo"selected";?>>Ngừng hoạt động</option>
                    </select>
                    Người đăng:<input type="text" value='<?php echo $ns[0]['username']; ?>' class="form-control" id="price" name='username' readonly>
                    Nội dung:
                  <div class="box-body pad">
                      <textarea name='content' class="textarea" placeholder="Place some text here"
                                style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $ns[0]['content']; ?></textarea>
                  </div>
                </div>
                      <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" name='submit' class="btn btn-default">Cập nhật</button>
                </div>
                </form>
            </div>
        </div>
        </div>
    </section>
  </div>
  <!-- /.content-wrapper -->
  <script>
  <?php if(isset($_GET['err']))
    ?>alert('<?php echo $_GET['err'];?>');<?php
  ?>
  
  </script>