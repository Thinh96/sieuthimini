<?php 
$type=new product_type();
$pt=$type->getbyId($id);
?><!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $title;?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>be/"></i> Trang chủ</a></li>
      </ol>
    </section>
    <section class='content'>
    <div class="row-auto">
        <!-- left column -->
        <div class='col-md-auto'>
            <div class="box box-primary">
                <div class="box-header with-border">
                <h3 class="box-title"></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" enctype="multipart/form-data" id='form1' method='post' action='<?php echo base_url();?>be/danh-muc/update/<?php echo $id;?>'>
                <div class="box-body">
                    <div class="form-group">
                    Tên danh mục:<input type="input" class="form-control" id="name" name='name' value='<?php echo $pt[0]['name'];?>'  placeholder="Nhập tên danh mục">
                    Danh mục cha:
                    <select name='parent' class='form-control'>
                    <option value='0' <?php if($pt[0]['parent_id']=='') echo 'selected'; ?>>Không có</option>
                    <?php 
                    $type=new product_type();
                    $pr=$type->getallParent();
                    foreach($pr as $v)
                    { ?>
                      <option value='<?php echo $v['id'];?>' <?php if($pt[0]['parent_id']==$v['id']) echo 'selected'; ?>><?php echo $v['name']; ?></option>
                      <?php
                    } 
                    ?>
                    </select>
                    Trạng thái:
                    <select name='status' class='form-control'>
                    <option value='1' <?php if($pt[0]['status']=='1') echo 'selected'; ?>>Hoạt động</option>
                    <option value='0' <?php if($pt[0]['status']=='0') echo 'selected'; ?>>Ngừng hoạt động</option>
                    </select>
                </div>
                <!-- /.box-body -->
                    
                <div class="box-footer">
                    <button type="submit" name='submit' class="btn btn-default">Cập nhật</button>
                </div>
                </form>
            </div>
        </div>
        </div>
    </section>
  </div>
  <!-- /.content-wrapper -->