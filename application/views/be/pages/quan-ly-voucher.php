<?php
$voucher=new voucher();
$lst=$voucher->getall();
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $title;?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>be/"></i> Trang chủ</a></li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            <div class="box-footer clearfix no-border">
              <a href='<?php echo base_url();?>be/them-voucher'><button type="button" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Thêm voucher</button></a>
            </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Mã voucher</th>
                  <th>Số lần dùng</th>
                  <th>Tỉ lệ</th>
                  <th>Khách hàng</th>
                  <th>Từ ngày</th>
                  <th>Đến ngày</th>
                  <th>Thao tác</th>
                </tr>
                </thead>
                <tbody>
                  <?php $i=1; foreach($lst as $k=>$v) {
                    ?>
                    <tr>
                      <td>
                      <?php   echo $v['id'];  ?>
                      </td>
                      <td><?php echo $v['amount'];?></td>
                      <td><?php echo $v['rate'];?>%</td>
                      <td><?php echo $v['user'];?></td>
                      <td><?php echo $v['from_date'];?></td>
                      <td><?php echo $v['to_date'];?></td>
                      <td><a href='<?php echo base_url();?>be/voucher/<?php echo $v['id']?>'><button><i class='fa fa-edit'></i></button></a>&nbsp<a href='<?php echo base_url();?>be/voucher/delete/<?php echo $v['id']?>'><button><i class='fa fa-close'></i></button></a></td>
                    </tr>
                    <?php
                    $i++;
                  }?>                
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  </div>
  <!-- /.content-wrapper -->