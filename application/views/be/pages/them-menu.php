<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $title;?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>be/"></i> Trang chủ</a></li>
        <li><a href="<?php echo base_url();?>be/them-menu"></i> Thêm menu</a></li>
      </ol>
    </section>
    <section class='content'>
    <div class="row-auto">
        <!-- left column -->
        <div class='col-md-auto'>   
            <div class="box box-primary">
                <div class="box-header with-border">
                <h3 class="box-title"></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" enctype="multipart/form-data" id='form1' method='post' action='<?php echo base_url();?>be/menu/insert'>
                <div class="box-body">
                    <div class="form-group">
                    <label for="name1">Tên:</label>
                    <input type="input" class="form-control" id="name" name='name' placeholder="Nhập tên menu">
                    <div style='width:0;height:0;overflow:hidden;'>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" name='submit' class="btn btn-default">Thêm</button>
                </div>
                </form>
            </div>
        </div>
        </div>
    </section>
  </div>
  <!-- /.content-wrapper -->