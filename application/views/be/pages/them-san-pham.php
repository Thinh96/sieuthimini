<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $title;?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>be/"></i> Trang chủ</a></li>
      </ol>
    </section>
    <section class='content'>
    <div class="row-auto">
        <!-- left column -->
        <div class='col-md-auto'>
            <div class="box box-primary">
                <div class="box-header with-border">
                <h3 class="box-title"></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" enctype="multipart/form-data" id='form1' method='post' action='<?php echo base_url();?>be/san-pham/insert'>
                <div class="box-body">
                    <div class="form-group">
                    Hình:<input type="file" class="form-control" id="hinh" name='hinh' >
                    Tên sản phẩm :<input type="input" class="form-control" id="name" name='name' placeholder="Nhập tên sản phẩm">
                    Giá bán<input type="input" class="form-control" id="price" name='price' placeholder="Nhập giá" pattern='^(0|[1-9][0-9]*)$' title='Chỉ nhập số'>
                    Mô tả sản phẩm :<br><textarea name='des' form='form1' rows=5 class='form-control'></textarea><br>
                    Loại sản phẩm(Danh mục):
                    <select class='form-control' name='type'>
                    <?php
                    $type=new product_type();
                    $pr=$type->getallParent();
                    foreach($pr as $k=>$v)
                    {
                      ?>
                      <option class='text-danger' disabled><?php echo $v['name'];?></option>
                      <?php
                      foreach($type->getallbyParent($v['id']) as $k1=>$v1)
                      {
                        ?>
                        <option value='<?php echo $v1['id'];?>'>&nbsp&nbsp<?php echo $v1['name'];?></option>
                        <?php
                      }
                    }
                    ?>
                    </select>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" name='submit' class="btn btn-default">Thêm</button>
                </div>
                </form>
            </div>
        </div>
        </div>
    </section>
  </div>
  <!-- /.content-wrapper -->
  <script>
  <?php if(isset($_GET['err']))
    ?>alert('<?php echo $_GET['err'];?>');<?php
  ?>
  
  </script>