<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $title;?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>be/"></i> Trang chủ</a></li>
      </ol>
    </section>
    <section class='content'>
    <div class="row-auto">
        <!-- left column -->
        <div class='col-md-auto'>
            <div class="box box-primary">
                <div class="box-header with-border">
                <h3 class="box-title"></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" enctype="multipart/form-data" id='form1' method='post' action='<?php echo base_url();?>be/chi-tiet-san-pham/insert/<?php echo $_GET['id'];?>'>
                <div class="box-body">
                    <div class="form-group">
                    Số lượng:<input type="number" min=1 class="form-control" id="amount" name='amount' placeholder="Số lượng" required>
                    Ngày sản xuất(nếu có):<input type="date" class="form-control" id="mfg" name='mfg' title='Chỉ nhập ngày'>
                    Ngày hết hạn(nếu có):<input type="date" class="form-control" id="exp" name='exp' title='Chỉ nhập ngày'>
                    Hạn bảo hành(nếu có):<input type="number" class="form-control" id="warr" name='warr' placeholder="Số ngày bảo hành" pattern='^(0|[1-9][0-9]*)$' title='Chỉ nhập số'>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" name='submit' class="btn btn-default">Thêm</button>
                </div>
                </form>
            </div>
        </div>
        </div>
    </section>
  </div>
  <!-- /.content-wrapper -->
  <script>
  <?php if(isset($_GET['err']))
    ?>alert('<?php echo $_GET['err'];?>');<?php
  ?>
  
  </script>