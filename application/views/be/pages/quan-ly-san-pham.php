<?php 
$product=new product();
$lst=$product->getall();
$role=$_SESSION['user_data']['role'];
?><!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $title;?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>be/"></i> Trang chủ</a></li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            <div class="box-footer clearfix no-border">
              <a href='<?php echo base_url();?>be/them-san-pham'><button type="button" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Thêm sản phẩm</button></a>
            </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Hình</th>
                  <th>Mã</th>
                  <th>Tên</th>
                  <th>Loại sản phẩm</th>
                  <th>Trạng thái</th>
                  <th>Thao tác</th>
                </tr>
                </thead>
                <tbody>
                  <?php $i=1; foreach($lst as $k=>$v) {
                    ?>
                    <tr>
                      <td><img src="<?php echo base_url();?>image/product/<?php echo $v['id'] ?>.png" width="75"></td>
                      <td><?php echo $v['id'];?></td>
                      <td><?php echo $v['name'];?>
                      <?php 
                      $p_d=new product_detail();
                      if(count($p_d->getbyProduct($v['id']))==0)
                      echo "<small class='text-danger'>(Hết hàng)</small>";
                      ?>
                      </td>
                      <td><?php
                      $type=new product_type();
                      $pt=$type->getbyId($v['product_type']);
                       echo $pt[0]['name'];
                      ?></td>
                      <td><?php 
                      switch($v['status']){
                        case 2:echo "Ngừng hoạt động"; break;
                        case 1:echo "Đang hoạt động"; break;
                      }
                      ?></td>
                      <td><a href='<?php echo base_url();?>be/san-pham/<?php echo $v['id']?>'><button title='Chi tiết'><i class='fa fa-edit'></i></button></a>&nbsp
                    <?php if($role>3){ ?><a href='<?php echo base_url();?>be/san-pham/delete/<?php echo $v['id']?>'><button title='Xóa'><i class='fa fa-close'></i></button></a><?php }?>&nbsp<a href='<?php echo base_url();?>be/them-khuyen-mai/<?php echo $v['id']?>'><button title='khuyến mại'><i class='fa fa-money'></i></button></a></td>
                    </tr>
                    <?php
                    $i++;
                  }?>                
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  </div>
  <!-- /.content-wrapper -->