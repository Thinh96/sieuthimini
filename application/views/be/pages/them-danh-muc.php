<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $title;?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>be/"></i> Trang chủ</a></li>
      </ol>
    </section>
    <section class='content'>
    <div class="row-auto">
        <!-- left column -->
        <div class='col-md-auto'>
            <div class="box box-primary">
                <div class="box-header with-border">
                <h3 class="box-title"></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" enctype="multipart/form-data" id='form1' method='post' action='<?php echo base_url();?>be/danh-muc/insert'>
                <div class="box-body">
                    <div class="form-group">
                    Tên danh mục:<input type="input" class="form-control" id="name" name='name'  placeholder="Nhập tên danh mục">
                    Danh mục cha:
                    <select name='parent' class='form-control'>
                    <option value='0'>Không có</option>
                    <?php 
                    $type=new product_type();
                    $pr=$type->getallParent();
                    foreach($pr as $v)
                    { ?>
                      <option value='<?php echo $v['id'];?>'><?php echo $v['name']; ?></option>
                      <?php
                    } 
                    ?>
                    </select>
                </div>
                <!-- /.box-body -->
                    
                <div class="box-footer">
                    <button type="submit" name='submit' class="btn btn-default">Thêm</button>
                </div>
                </form>
            </div>
        </div>
        </div>
    </section>
  </div>
  <!-- /.content-wrapper -->