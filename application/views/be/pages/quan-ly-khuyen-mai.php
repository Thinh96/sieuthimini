<?php
$pro=new promotion();
$lst=$pro->getall();
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $title;?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>be/"></i> Trang chủ</a></li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            <div class="box-footer clearfix no-border">
              <a href='<?php echo base_url();?>be/them-khuyen-mai'><button type="button" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Thêm khuyến mại</button></a>
            </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Mã sản phẩm</th>
                  <th>Tên sản phẩm</th>
                  <th>Tỉ lệ</th>
                  <th>Số lượng còn lại</th>
                  <th>Từ ngày</th>
                  <th>Đến ngày</th>
                  <th>Thao tác</th>
                </tr>
                </thead>
                <tbody>
                  <?php $i=1; foreach($lst as $k=>$v) {
                    ?>
                    <tr>
                      <td>
                      <?php 
                      $p=new product();
                      $product=$p->getbyID($v['product']);
                      echo $product[0]['id'];
                      ?>
                      </td>
                      <td><?php echo $product[0]['name'];;?></td>
                      <td><?php echo $v['rate'];?>%</td>
                      <td><?php echo $v['amount'];?></td>
                      <td><?php echo $v['from_date'];?></td>
                      <td><?php echo $v['to_date'];?></td>
                      <td><a href='<?php echo base_url();?>be/khuyen-mai/<?php echo $v['id']?>'><button><i class='fa fa-edit'></i></button></a>&nbsp<a href='<?php echo base_url();?>be/khuyen-mai/delete/<?php echo $v['id']?>'><button><i class='fa fa-close'></i></button></a></td>
                    </tr>
                    <?php
                    $i++;
                  }?>                
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  </div>
  <!-- /.content-wrapper -->