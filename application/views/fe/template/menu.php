<!-- header fixed -->
<div class="wrap_header fixed-header2 trans-0-4" >
		<!-- Logo -->
		<a style='font-family:Arial, Helvetica, sans-serif'href="index.html" class="logo">
			<img src="<?php echo base_url();?>image/logo.png" alt="IMG-LOGO">
		</a>

		<!-- Menu -->
		<div class="wrap_menu">
			<nav class="menu">
				<ul class="main_menu">
					<li>
						<a style='font-family:Arial, Helvetica, sans-serif'href="<?php echo base_url();?>">Trang chủ</a>
					</li>

					<li>
						<a style='font-family:Arial, Helvetica, sans-serif'href="<?php echo base_url();?>danh-sach-san-pham">Danh mục</a>
                        <ul class="sub_menu">
                        <?php $menu=new menu();
                        foreach($menu->getall() as $v){
                            ?>
                            <li><a style='font-family:Arial, Helvetica, sans-serif'href="<?php echo base_url()."danh-sach-san-pham/".$v['link'];?>"><?php echo $v['name'];?></a></li>
                            <?php
                        }
                        ?>
							
						</ul>
					</li>
					<li>
						<a style='font-family:Arial, Helvetica, sans-serif'href="<?php echo base_url();?>danh-sach-tin-tuc">Tin tức</a>
					</li>   

					<li>
						<a style='font-family:Arial, Helvetica, sans-serif'href="<?php echo base_url();?>lien-he">Liên hệ</a>
					</li>

					<li>
						<a style='font-family:Arial, Helvetica, sans-serif'href="<?php echo base_url();?>cau-hoi-thuong-gap">FAQ</a>
					</li>
				</ul>
			</nav>
		</div>

		<!-- Header Icon -->
		<div class="header-icons">
			<a style='font-family:Arial, Helvetica, sans-serif' 	href="<?php echo base_url();?><?php if($_SESSION['user_data']['role']==1 ) echo 'dang-nhap'; else echo 'thong-tin-ca-nhan'; ?>" class="header-wrapicon1 dis-block">
				<img src="<?php echo base_url();?>bootstrap/fe/images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
			</a>

			<span class="linedivide1"></span>

			<div class="header-wrapicon2">
				<img src="<?php echo base_url();?>bootstrap/fe/images/icons/icon-header-02.png" class="header-icon1 js-show-header-dropdown" alt="ICON">
				<span class="header-icons-noti"><?php echo $_SESSION['user_data']['cart']['amount'];?></span>

				<!-- Header cart noti -->
				<div class="header-cart header-dropdown">
					<ul class="header-cart-wrapitem">
                    <?php 
                    foreach($_SESSION['user_data']['cart']['list'] as $v)
                    { $p=new product();
                        $pd=$p->getbyId($v['id']);
                        ?>
                        <li class="header-cart-item">
							<div class="header-cart-item-img">
								<img src="<?php echo base_url();?>image/product/<?php echo $v['id']?>.png" alt="IMG">
							</div>

							<div class="header-cart-item-txt">
								<a style='font-family:Arial, Helvetica, sans-serif'href="#" class="header-cart-item-name">
									<?php echo $pd[0]['name'];?>
								</a>

								<span class="header-cart-item-info">
                                <?php echo $v['amount'];?> x <?php echo number_format($pd[0]['price']);?> VND
								</span>
							</div>
						</li>
                        <?php
                    }
                    ?>
					</ul>

					<div class="header-cart-total">
                    <?php echo number_format($_SESSION['user_data']['cart']['total']);?> VND
					</div>

					<div class="header-cart-buttons">
						<div class="header-cart-wrapbtn">
							<!-- Button -->
							<a style='font-family:Arial, Helvetica, sans-serif'href="<?php echo base_url();?>gio-hang" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
								Xem giỏ
							</a>
						</div>

						<div class="header-cart-wrapbtn">
							<!-- Button -->
							<a style='font-family:Arial, Helvetica, sans-serif'href="<?php echo base_url();?>thanh-toan" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
								Thanh toán
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	
	<!-- Header -->
	<header class="header2">
		<!-- Header desktop -->
		<div class="container-menu-header-v2 p-t-26">
			<div class="topbar2">
				<div class="topbar-social">
					<a href="http://<?php $display=new display(); $fb=$display->searchbyName('fb');echo $fb[0]['content'];?>" class="topbar-social-item fa fa-facebook"></a>
				</div>

				<!-- Logo2 -->
				<a style='font-family:Arial, Helvetica, sans-serif'href="<?php echo base_url();?>" class="logo2">
					<img src="<?php echo base_url();?>image/logo.png" alt="IMG-LOGO">
				</a>

				<div class="topbar-child2">
					<!--  -->
					<a style='font-family:Arial, Helvetica, sans-serif'href="<?php echo base_url();?><?php if($_SESSION['user_data']['role']==1 )echo 'dang-nhap'; else echo 'thong-tin-ca-nhan'; ?>" class="header-wrapicon1 dis-block">
				        <img src="<?php echo base_url();?>bootstrap/fe/images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
			        </a>

			            <span class="linedivide1"></span>
					<div class="header-wrapicon2 m-r-13">
						<img src="<?php echo base_url();?>bootstrap/fe/images/icons/icon-header-02.png" class="header-icon1 js-show-header-dropdown" alt="ICON">
						<span class="header-icons-noti"><?php echo $_SESSION['user_data']['cart']['amount'];?></span>

						<!-- Header cart noti -->
						<div class="header-cart header-dropdown">
					<ul class="header-cart-wrapitem">
                    <?php 
                    foreach($_SESSION['user_data']['cart']['list'] as $v)
                    { $p=new product();
                        $pd=$p->getbyId($v['id']);
                        ?>
                        <li class="header-cart-item">
							<div class="header-cart-item-img">
								<img src="<?php echo base_url();?>image/product/<?php echo $v['id']?>.png" alt="IMG">
							</div>

							<div class="header-cart-item-txt">
								<a style='font-family:Arial, Helvetica, sans-serif'href="#" class="header-cart-item-name">
									<?php echo $pd[0]['name'];?>
								</a>

								<span class="header-cart-item-info">
                                <?php echo $v['amount'];?> x <?php echo number_format($pd[0]['price']);?> VND
								</span>
							</div>
						</li>
                        <?php
                    }
                    ?>
					</ul>

					<div class="header-cart-total">
                    <?php echo number_format($_SESSION['user_data']['cart']['total']);?> VND
					</div>

					<div class="header-cart-buttons">
						<div class="header-cart-wrapbtn">
							<!-- Button -->
							<a style='font-family:Arial, Helvetica, sans-serif'href="<?php echo base_url();?>gio-hang" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
								Xem giỏ
							</a>
						</div>

						<div class="header-cart-wrapbtn">
							<!-- Button -->
							<a style='font-family:Arial, Helvetica, sans-serif'href="<?php echo base_url();?>thanh-toan" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
								Thanh toán
							</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="wrap_header">

				<!-- Menu -->
				<div class="wrap_menu">
					<nav class="menu">
						<ul class="main_menu">
                        <li>
						<a style='font-family:Arial, Helvetica, sans-serif'href="<?php echo base_url();?>">Trang chủ</a>
					</li>

					<li>
						<a style='font-family:Arial, Helvetica, sans-serif'href="<?php echo base_url();?>danh-sach-san-pham">Danh mục</a>
                        <ul class="sub_menu">
                        <?php $menu=new menu();
                        foreach($menu->getall() as $v){
                            ?>
                            <li><a style='font-family:Arial, Helvetica, sans-serif'href="<?php echo base_url()."danh-sach-san-pham/".$v['link'];?>"><?php echo $v['name'];?></a></li>
                            <?php
                        }
                        ?>
							
						</ul>
					</li>

					
					<li>
						<a style='font-family:Arial, Helvetica, sans-serif'href="<?php echo base_url();?>danh-sach-tin-tuc">Tin tức</a>
					</li>   

					<li>
						<a style='font-family:Arial, Helvetica, sans-serif'href="<?php echo base_url();?>lien-he">Liên hệ</a>
					</li>

					<li>
						<a style='font-family:Arial, Helvetica, sans-serif'href="<?php echo base_url();?>cau-hoi-thuong-gap">FAQ</a>
					</li>
						</ul>
					</nav>
				</div>

				<!-- Header Icon -->
				<div class="header-icons">

				</div>
			</div>
		</div>

		<!-- Header Mobile -->
		<div class="wrap_header_mobile">
			<!-- Logo moblie -->
			<a style='font-family:Arial, Helvetica, sans-serif'href="index.html" class="logo-mobile">
				<img src="<?php echo base_url();?>image/logo.png" alt="IMG-LOGO">
			</a>

			<!-- Button show menu -->
			<div class="btn-show-menu">
				<!-- Header Icon mobile -->
				<div class="header-icons-mobile">
                <a style='font-family:Arial, Helvetica, sans-serif'href="<?php echo base_url();?><?php if($_SESSION['user_data']['role']==1 )echo 'dang-nhap'; else echo 'thong-tin-ca-nhan'; ?>" class="header-wrapicon1 dis-block">
				        <img src="<?php echo base_url();?>bootstrap/fe/images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
			        </a>

					<span class="linedivide2"></span>

					<div class="header-wrapicon2">
						<img src="<?php echo base_url();?>bootstrap/fe/images/icons/icon-header-02.png" class="header-icon1 js-show-header-dropdown" alt="ICON">
						<span class="header-icons-noti"><?php echo $_SESSION['user_data']['cart']['amount'];?></span>

						<!-- Header cart noti -->
				<div class="header-cart header-dropdown">
					<ul class="header-cart-wrapitem">
                    <?php 
                    foreach($_SESSION['user_data']['cart']['list'] as $v)
                    { $p=new product();
                        $pd=$p->getbyId($v['id']);
                        ?>
                        <li class="header-cart-item">
							<div class="header-cart-item-img">
								<img src="<?php echo base_url();?>image/product/<?php echo $v['id']?>.png" alt="IMG">
							</div>

							<div class="header-cart-item-txt">
								<a style='font-family:Arial, Helvetica, sans-serif'href="#" class="header-cart-item-name">
									<?php echo $pd[0]['name'];?>
								</a>

								<span class="header-cart-item-info">
                                <?php echo $v['amount'];?> x <?php echo number_format($pd[0]['price']);?> VND
								</span>
							</div>
						</li>
                        <?php
                    }
                    ?>
					</ul>

					<div class="header-cart-total">
                    <?php echo number_format($_SESSION['user_data']['cart']['total']);?> VND
					</div>

					<div class="header-cart-buttons">
						<div class="header-cart-wrapbtn">
							<!-- Button -->
							<a style='font-family:Arial, Helvetica, sans-serif'href="<?php echo base_url();?>gio-hang" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
								Xem giỏ
							</a>
						</div>

						<div class="header-cart-wrapbtn">
							<!-- Button -->
							<a style='font-family:Arial, Helvetica, sans-serif'href="<?php echo base_url();?>thanh-toan" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
								Thanh toán
							</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="btn-show-menu-mobile hamburger hamburger--squeeze">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</div>
			</div>
		</div>

		<!-- Menu Mobile -->
		<div class="wrap-side-menu" >
			<nav class="side-menu">
				<ul class="main-menu">
					<li class="item-topbar-mobile p-l-20 p-t-8 p-b-8">
						<div class="topbar-child2-mobile">
							<span class="topbar-email">
								fashe@example.com
							</span>

							<div class="topbar-language rs1-select2">
								<select class="selection-1" name="time">
									<option>USD</option>
									<option>EUR</option>
								</select>
							</div>
						</div>
					</li>

					<li class="item-topbar-mobile p-l-10">
						<div class="topbar-social-mobile">
							<a style='font-family:Arial, Helvetica, sans-serif' href="<?php $display=new display(); $fb=$display->searchbyName('fb');echo $fb[0]['content'];?>" class="topbar-social-item fa fa-facebook"></a></div>
					</li>

                    <li class="item-menu-mobile">
						<a style='font-family:Arial, Helvetica, sans-serif'href="<?php echo base_url();?>">Trang chủ</a>
					</li>

					<li class="item-menu-mobile">
						<a style='font-family:Arial, Helvetica, sans-serif' href="<?php echo base_url();?>danh-sach-san-pham">Danh mục</a>
                        <ul class="sub_menu">
                        <?php $menu=new menu();
                        foreach($menu->getall() as $v){
                            ?>
                            <li><a style='font-family:Arial, Helvetica, sans-serif'href="<?php echo base_url()."danh-sach-san-pham/".$v['link'];?>"><?php echo $v['name'];?></a></li>
                            <?php
                        }
                        ?>
							
						</ul>
					</li class="item-menu-mobile">


					<li class="item-menu-mobile">
						<a style='font-family:Arial, Helvetica, sans-serif'href="<?php echo base_url();?>danh-sach-tin-tuc">Tin tức</a>
					</li>   

					<li class="item-menu-mobile">
						<a style='font-family:Arial, Helvetica, sans-serif'href="<?php echo base_url();?>lien-he">Liên hệ</a>
					</li>

					<li class="item-menu-mobile">
						<a style='font-family:Arial, Helvetica, sans-serif'href="<?php echo base_url();?>cau-hoi-thuong-gap">FAQ</a>
					</li>
				</ul>
			</nav>
		</div>
	</header>