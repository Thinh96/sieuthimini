<?php
if($id!="")
{
    $type=new product_type();
    $menu=new menu();
    $t=$menu->searchlikeName($id);
    if(count($t)>0)
    {
        $tp=$type->searchlikePName($t[0]['name']);
        if(count($tp)>0) $_GET['type']=$tp[0]['id'];
        else{
            $tp=$type->searchlikeName($t[0]['name']);
            if(count($tp)>0)
            {
                $_GET['p_type']=$tp[0]['id'];
                $_GET['type']=$tp[0]['parent_id'];
            } 
        }
    }
}
if(!isset($_GET['page'])) 
{
    $max=15;$min=0;
}
else{
    $max=$_GET['page']*15;
    $min=($_GET['page']-1)*15;
}
$p=new product();
$type=new product_type();
if(isset($_GET['s']))
{
    $lst=$p->search($_GET['s']);
}
if(isset($_GET['type']) && !isset($_GET['p_type']))
{
    $lst=array();
    $lst_t=$type->getallbyParent($_GET['type']);
    $i=0;
    foreach($lst_t as $k=>$v)
    {
        if($v['status']!=1) continue;
        foreach($p->getbyType($v['id']) as $k2=>$v2)
        {
            if($v2['status']!=1) continue;
            $lst[$i]=$v2;
            $i++;
        }
    }
}
if(isset($_GET['type']) && isset($_GET['p_type']))
{
    $lst=$p->getbyType($_GET['p_type']);
}
if(!isset($_GET['type']) && !isset($_GET['p_type']) && !isset($_GET['s']))
{
    $lst=$p->getall();
    
}
?>
<!-- Title Page -->
<section class="bg-title-page p-t-50 p-b-40 flex-col-c-m" style="background-image: url(<?php echo base_url();?>image/title.png);">
		<h2 class="l-text2 t-center">
			Danh sách sản phẩm
		</h2>
	</section>
<!-- Content page -->
<section class="bgwhite p-t-55 p-b-65">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-md-4 col-lg-3 p-b-50">
					<div class="leftbar p-r-20 p-r-0-sm">
						<!--  -->
						<h4 class="m-text14 p-b-7">
							DANH MỤC
						</h4>

						<ul class="p-b-54">
                            <li class="p-t-4">
								<a href="<?php echo base_url();?>danh-sach-san-pham" class="s-text13 <?php if(!isset($_GET['type'])) echo'active1';?>">
									Tất cả
								</a>
							</li>
                            <?php $type=new product_type();
                            foreach($type->getallParent() as $v)
                            {
                                if($v['status']!=1) continue;
                                ?>
                                <li class="p-t-4">
								<a href="<?php echo base_url();?>danh-sach-san-pham?type=<?php echo $v['id'];?>" class="s-text13 <?php if(isset($_GET['type'])) { if($_GET['type']==$v['id']) echo 'active1';}?>">
									<?php echo $v['name'];?>
								</a>
							</li>
                                <?php
                            }
                            ?>
							

                        </ul>
                        <?php 
                        if(isset($_GET['type'])){
                        ?>
                        <!--  -->
                        <h4 class="m-text14 p-b-7">
							LOẠI SẢN PHẨM
						</h4>

						<ul class="p-b-54">
                            <li class="p-t-4">
								<a href="#" class="s-text13 class="s-text13 <?php if(!isset($_GET['p_type'])) echo'active1';?>"">
									Tất cả
								</a>
							</li>
                            <?php $type=new product_type();
                            foreach($type->getallbyParent($_GET['type']) as $v)
                            {
                                ?>
                                <li class="p-t-4">
								<a href="<?php echo base_url();?>danh-sach-san-pham?type=<?php echo $_GET['type'];?>&p_type=<?php echo $v['id'];?>" class="s-text13 <?php if(isset($_GET['p_type'])) if($_GET['p_type']==$v['id']) echo 'active1';?>">
									<?php echo $v['name'];?>
								</a>
							</li>
                                <?php
                            }
                            ?>
							

                        </ul><?php } ?>
						<!--  -->
						
					</div>
				</div>

				<div class="col-sm-6 col-md-8 col-lg-9 p-b-50">
					<!--  -->
					<div class="flex-sb-m flex-w p-b-35">
						<div class="flex-w">
                            <form id='form1'></form>
							<div class="rs2-select2 bo4 of-hidden w-size12 m-t-5 m-b-5 m-r-10">
								<input form='form1' type='search' name='s' class='form-control' placeholder='Nhập tên sản phẩm'>
							</div>
                            <div class="rs2-submit of-hidden m-t-5 m-b-5 m-r-10">
								<input form='form1' type='submit' class='btn btn-default' value='Tìm kiếm'>
							</div>
						</div>

						<span class="s-text8 p-t-5 p-b-5">
                        Từ <?php echo $min+1;?> đến <?php if(count($lst)<$max) echo count($lst); else echo $max;?> Trong tổng <?php echo count($lst);?>
						</span>
					</div>

					<!-- Product -->
					<div class="row">
                    <?php 
							foreach($lst as $k=>$v)
							{
								if($k<$max && $k>=$min)
								{ 
                                    $detail=new product_detail();
                                    $amount=$detail->getamountbyProduct($v['id']);
									$pro=new promotion();
									$pr=$pro->getallbyProduct($v['id']);
									if(count($pr)>=1)
									$rate=$pr[0]['rate'];
									else $rate=0;
									?>
									<div class="col-sm-6 col-md-4 col-lg-3 p-b-50">
									<!-- Block2 -->
									<div class="block2">
										<div class="block2-img wrap-pic-w of-hidden pos-relative block2-<?php if(time()-strtotime($v['created_at'])<7*24*60*60) echo 'labelnew'; if($rate > 0) echo 'labelsale';?>">
											<img src="<?php echo base_url();?>image/product/<?php echo $v['id'];?>.png" alt="IMG-PRODUCT">
                                            <?php if($amount[0]['total'] !=0){?>
											<div class="block2-overlay trans-0-4">
												<div class="block2-btn-addcart w-size1 trans-0-4">
                                                    <!-- Button -->
													<a href='<?php echo base_url();?>gio-hang/insert/<?php echo $v['id'];?>'><button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4">
														Thêm vào giỏ
													</button>
													</a>
												</div>
                                            </div> <?php } ?>
										</div>
	
										<div class="block2-txt p-t-20">
											<a href="<?php echo base_url()?>san-pham/<?php echo $v['id'];?>" class="block2-name dis-block s-text3 p-b-5">
                                                <big><?php echo $v['name']; ?></big>
                                                <b class='text-danger'><?php if($rate>0) echo "(-$rate%)" ?></b>
                                                <b class='text-danger'><?php if($amount[0]['total'] ==0) echo "(HẾT HÀNG)" ?></b>
											</a>
	
											<span class="block2-price m-text6 p-r-5">
												<?php 
												if($rate > 0)
												{
													?>
													<b style='text-decoration: line-through;'><?php echo number_format($v['price']); ?> VND</b> <br>
													<b class='text-danger'><?php echo number_format(round($v['price']*(100-$rate)/100 , -3	)); ?> VND</b> 
													<?php
												}
												else
												{
													?>
													<b class='text-danger'><?php echo number_format($v['price']); ?>VND</b> 
													<?php
												}
												?>
											</span>
										</div>
									</div>
								</div><?php
								}
							}
                            ?>                                
					</div>
                    <?php 
                    $total_p=intval(count($lst)/20);
                    ?>
                    <!-- Pagination -->
                    <?php 
                    if($total_p > 0 ){
                    ?>
                    <div class="pagination flex-m flex-w p-t-26">
                    <?php
                    for($i=0;$i<=$total_p;$i++)
                    {
                        ?>
                        <a href="<?php echo base_url();?>danh-sach-san-pham?page=<?php echo $i+1; if(isset($_GET['type'])) echo "&type=".$_GET['type']; if(isset($_GET['p_type'])) echo "&p_type=".$_GET['p_type']; if(isset($_GET['s'])) echo "&s=".$_GET['s'];?>" class="item-pagination flex-c-m trans-0-4 <?php if(!isset($_GET['page'])){if($i==0) echo 'active-pagination';}  else if($i+1==$_GET['page']) echo 'active-pagination';?>"><?php echo $i+1;?></a>
                        <?php
                    }
                    }
                    ?>
					</div>
				</div>
			</div>
		</div>
	</section>