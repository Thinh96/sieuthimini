<div class='container-fluid' style='min-height:500px;background:url(<?php echo base_url();?>image/login_background) no-repeat; background-size:1400px 500px;background-position:center;'>
    <h2 class='text-primary' style='text-align:center'>Đăng kí tài khoản</h2>
    <form method="post" action="<?php echo base_url();?>dang-ki/insert" style='border-radius:5px;background:rgba(0,0,0,0.75);padding:10px;margin:0 auto;'>
        <div class='row'><div class='col-md-6'>
            <div class='form-group text-light'>
                <label for="username">Tài khoản :</label>
                <input type='text' class='form-control' name="username" id="username" placeholder="tài khoản đăng nhập" required>
            </div>
            <div class='form-group text-light'>
                <label for="password">Mật khẩu:</label>
                <input type='password' class='form-control' name="password" id="password" placeholder="Mật khẩu" pattern=".{6,}"  title="6 kí tự trở lên" required>
            </div>
            <div class='form-group text-light'>
                <label for="password">Nhập lại mật khẩu:</label>
                <input type='password' class='form-control' name="re-password" id="re-password" placeholder="Lặp lại mật khẩu" pattern=".{6,}"  title="6 kí tự trở lên" required>
            </div>
            <div class='form-group text-light'>
                <label for="password">Họ tên:</label>
                <input type='text' class='form-control' name="name" id="name" placeholder="Họ và tên" pattern=".{6,}"  title="6 kí tự trở lên" required>
            </div>
            <div class='form-group text-light'>
                <label for="password">giới tính:</label>
                <select name='gender' class='form-control'>
                <option value='1'>Nam</option><option value='1'>Nữ</option>
                </select>
            </div>
            <small class='text-danger'><?php if(isset($error)) echo $error; ?></small>
        </div>
        <div class='col-md-6'>
            <div class='form-group text-light'>
                <label for="username">Ngày sinh:</label>
                <input type='date' class='form-control' name="birth" id="birth" required>
            </div>
            <div class='form-group text-light'>
                <label for="password">Địa chỉ:</label>
                <input type='text' class='form-control' name="add" id="add" placeholder="Địa chỉ" pattern=".{6,}"  title="6 kí tự trở lên" required>
            </div>
            <div class='form-group text-light'>
                <label for="password">Số điện thoại:</label>
                <input type='number' class='form-control' name="phone" id="phone" placeholder="Số điện thoại"  required >
            </div>
            <div class='form-group text-light'>
                <label for="password">Mail:</label>
                <input type='text' class='form-control' name="mail" id="mail" placeholder="Example@example.com" required>
            </div>
            <div class='form-group text-light'>
                <label for="password">Số chứng minh:</label>
                <input type='number' class='form-control' name="id_card" id="id_card" placeholder="Mật khẩu" pattern=".{6,}"  title="6 kí tự trở lên" required>
            </div>
            <small class='text-danger'><?php if(isset($error)) echo $error; ?></small>
            <div class='form-group text-dark'>
                <input type='submit' class='btn btn-primary' name="login_sm" value="Đăng kí">
            </div>
        </div></div>
    </form>
    
</div>