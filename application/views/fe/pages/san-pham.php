<?php 
$p=new product();
$product=$p->getbyId($id);
?>
    <!-- Product Detail -->
	<div class="container bgwhite p-t-35 p-b-80">
		<div class="flex-w flex-sb">
			<div class="w-size13 p-t-30 respon5">
				<div class="wrap-slick3 flex-sb flex-w">
					<div class="wrap-slick3-dots"></div>

					<div class="slick3">
						<div class="item-slick3" data-thumb="<?php echo base_url();?>image/product/<?php echo $product[0]['id'];?>">
							<div class="wrap-pic-w">
								<img src="<?php echo base_url();?>image/product/<?php echo $product[0]['id'];?>" alt="IMG-PRODUCT">
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="w-size14 p-t-30 respon5">
				<h4 class="product-detail-name m-text16 p-b-13">
					<?php echo $product[0]['name'];?>
				</h4>

				<span class="m-text17">
                <?php echo number_format($product[0]['price']);?> VND
				</span>

				<p class="s-text8 p-t-10">
					
				</p>

				<!--  -->
				<div style='width:0;overflow:hidden;'>
				<form id='form1' method='post' action="<?php echo base_url();?>gio-hang/insert/<?php echo $product[0]['id']; ?>">
				</div>
				<div class="p-t-33 p-b-60">

					<div class="flex-r-m flex-w p-t-10">
						<div class="w-size16 flex-m flex-w">
						<?php 
						$p_d=new product_detail();
						$am=0;
                        $am=$p_d->getamountbyProduct($id);
                        if($am[0]['total']>0){echo 'Số lượng còn lại:'.$am[0]['total'];}
						?>
						</div>
                        
						<div class="w-size16 flex-m flex-w">Số lượng:
							<?php if($am[0]['total']>0){?>
                            <div class="flex-w bo5 of-hidden m-r-22 m-t-10 m-b-10">
								<button class="btn-num-product-down color1 flex-c-m size7 bg8 eff2">
									<i class="fs-12 fa fa-minus" aria-hidden="true"></i>
								</button>
                                
								<input class="size8 m-text18 t-center num-product" form='form1' type="number" name="amount" value="1">

								<button  class="btn-num-product-up color1 flex-c-m size7 bg8 eff2">
									<i class="fs-12 fa fa-plus" aria-hidden="true"></i>
								</button>
							</div>
                        <?php } else echo '0';?>

							<div class="btn-addcart-product-detail size9 trans-0-4 m-t-10 m-b-10">
								<!-- Button -->
								<?php if($am[0]['total']>0){?><button form='form1' name='submit' class="flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4">
									Thêm vào giỏ
								</button><?php }?>	
							</div>
						</div>
					</div>
				</div>

				<div class="p-b-45">
					<span class="s-text8 m-r-35"></span>
					<span class="s-text8"></span>
				</div>

				<!--  -->
				<div class="wrap-dropdown-content bo6 p-t-15 p-b-14 active-dropdown-content">
					<h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4">
						Mô tả:
						<i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
						<i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
					</h5>

					<div class="dropdown-content dis-none p-t-15 p-b-23">
						<p class="s-text8">
							<?php echo $product[0]['description'];?>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>

<!-- Relate Product -->
<section class="relateproduct bgwhite p-t-45 p-b-138">
		<div class="container">
			<div class="sec-title p-b-60">
				<h3 class="m-text5 t-center">
					Sản phẩm liên quan
				</h3>
			</div>

			<!-- Slide2 -->
			<div class="wrap-slick2">
				<div class="slick2">
                <?php foreach($p->getnew() as $k=>$v){
                    $detail=new product_detail();
                    $amount=$detail->getamountbyProduct($v['id']);
                    $pro=new promotion();
					$pr=$pro->getallbyProduct($v['id']);
					if(count($pr)>=1)
					$rate=$pr[0]['rate'];
					else $rate=0;
                    ?>
                    <div class="item-slick2 p-l-15 p-r-15">
						<!-- Block2 -->
						<div class="block2">
							<div class="block2-img wrap-pic-w of-hidden pos-relative block2-labelnew">
								<img src="<?php echo base_url();?>image/product/<?php echo $v['id'];?>" alt="IMG-PRODUCT">

								<?php if($amount[0]['total'] !=0){?>
											<div class="block2-overlay trans-0-4">
												<div class="block2-btn-addcart w-size1 trans-0-4">
                                                    <!-- Button -->
													<a href='<?php echo base_url();?>gio-hang/insert/<?php echo $v['id'];?>'><button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4">
														Thêm vào giỏ
													</button>
													</a>
												</div>
                                            </div> <?php } ?>
							</div>

							<div class="block2-txt p-t-20">
								<a href="product-detail.html" class="block2-name dis-block s-text3 p-b-5">
									<?php echo $v['name']; ?>
                                    <b class='text-danger'><?php if($amount[0]['total'] ==0) echo "(HẾT HÀNG)" ?></b>
								</a>

								<span class="block2-price m-text6 p-r-5">
                                <?php 
												if($rate > 0)
												{
													?>
													<b style='text-decoration: line-through;'><?php echo number_format($v['price']); ?> VND</b> 
													<b class='text-danger'><?php echo number_format(round($v['price']*(100-$rate)/100 , -3	)); ?> VND</b> 
													<?php
												}
												else
												{
													?>
													<b class='text-danger'><?php echo number_format($v['price']); ?>VND</b> 
													<?php
												}
												?>
								</span>
							</div>
						</div>
					</div>
<?php }?>
					

				</div>
			</div>

		</div>
	</section>
