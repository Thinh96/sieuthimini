<!-- content page -->
<section class="bgwhite p-t-66 p-b-38">
		<div class="container">
			<div class="row">
				<div class="col-md-4 p-b-30">
					<div class="hov-img-zoom" style='width:380px'>
                    <?php 
                    $display=new display();
                    $map=$display->searchbyName("Map");
                    echo $map[0]['content'];
                    ?>
					</div>
				</div>

				<div class="col-md-8 p-b-30">
					<h3 class="m-text26 p-t-15 p-b-16">
						SIÊU THỊ MINI
					</h3>

					<p class="p-b-28">
                        <?php 
                        $mota=$display->searchbyName('Mô tả');
                        echo $mota[0]['content'];
                        ?>
					</p>

					<div class="bo13 p-l-29 m-l-9 p-b-10">
						<p class="p-b-11">
							Thông tin liên lạc:<br>
                            <?php $add=$display->searchbyName('Địa chỉ');
                                $phone=$display->searchbyName('Điện thoại');
                            ?>
                            Địa chỉ: <?php echo $add[0]['content'];?><br>
                            Số điện thoại: <?php echo $phone[0]['content'];?>
						</p>

					</div>
				</div>
			</div>
		</div>
	</section>