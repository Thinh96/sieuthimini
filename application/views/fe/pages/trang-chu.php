<!-- Slide1 -->
<section class="slide1">
		<div class="wrap-slick1">
			<div class="slick1">
                <?php
                $slider=new slider();
                foreach($slider->getall() as $v)
                {
                    ?>
                    <div class="item-slick1 item1-slick1" style="background-image: url(<?php echo base_url();?>image/slider_<?php echo $v['id'];?>.png);">
					<div class="wrap-content-slide1 sizefull flex-col-c-m p-l-15 p-r-15 p-t-150 p-b-170">
						<h2 style='font-family:arial' class="caption1-slide1 xl-text2 t-center bo14 p-b-6 animated visible-false m-b-22" data-appear="fadeInUp">
							<?php echo $v['title'];?>
						</h2>

						<span style='font-family:arial' class="caption2-slide1 m-text1 t-center animated visible-false m-b-33" data-appear="fadeInDown">
                            <?php echo $v['small'];?>
						</span>

						<div  class="wrap-btn-slide1 w-size1 animated visible-false" data-appear="zoomIn">
							<!-- Button -->
							<a style='font-family:arial' href="<?php echo base_url();?>danh-sach-san-pham" class="flex-c-m size2 bo-rad-23 s-text2 bgwhite hov1 trans-0-4">
								Xem ngay
							</a>
						</div>
					</div>
				</div>
                    <?php
                }
                ?>

			</div>
		</div>
	</section>
	<!-- Banner -->
	<div class="banner bgwhite p-t-40 p-b-40">
		<div class="container">
			<div class="row">
                <?php 
                $type=new product_type();
                foreach($type->getallParent() as $v)
                {
                    ?>
                    <div class="col-sm-10 col-md-8 col-lg-4 m-l-r-auto">
					<!-- block1 -->
					<div class="block1 hov-img-zoom pos-relative m-b-30">
						<img src="<?php echo base_url();?>image/type_<?php echo $v['id'];?>.png" alt="IMG-BENNER">

						<div class="block1-wrapbtn w-size2">
							<!-- Button -->
							<a style='font-family:arial' href="<?php echo base_url();?>danh-sach-san-pham?type=<?php echo $v['id'];?>" class="flex-c-m size2 m-text2 bg3 hov1 trans-0-4">
								<b><?php echo $v['name']?></b>
							</a>
						</div>
					</div>
				</div>
                    <?php
                }
                ?>
			</div>
		</div>
	</div>


	<!-- Our product -->
	<section class="bgwhite p-t-45 p-b-58">
		<div class="container">
			<div class="sec-title p-b-22">
				<h3 style='font-family:arial' class="m-text5 t-center">
					SẢN PHẨM MỚI
				</h3>
			</div>

			<!-- Tab01 -->
			<div class="tab01">
				<!-- Nav tabs -->

				<!-- Tab panes -->
				<div class="tab-content p-t-35">
					<!-- - -->
					<div class="tab-pane fade show active" id="best-seller" role="tabpanel">
						<div class="row">
                            <?php 
                            $p=new product();
							foreach($p->getnew() as $k=>$v)
							{
								if($k<8)
								{ 
									$detail=new product_detail();
                                    $amount=$detail->getamountbyProduct($v['id']);
									$pro=new promotion();
									$pr=$pro->getallbyProduct($v['id']);
									if(count($pr)>=1)
									$rate=$pr[0]['rate'];
									else $rate=0;
									?>
									<div class="col-sm-6 col-md-4 col-lg-3 p-b-50">
									<!-- Block2 -->
									<div class="block2">
										<div class="block2-img wrap-pic-w of-hidden pos-relative block2-labelnew">
											<img src="<?php echo base_url();?>image/product/<?php echo $v['id'];?>.png" alt="IMG-PRODUCT">
	
											<?php if($amount[0]['total'] !=0){?>
											<div class="block2-overlay trans-0-4">
												<div class="block2-btn-addcart w-size1 trans-0-4">
                                                    <!-- Button -->
													<a href='<?php echo base_url();?>gio-hang/insert/<?php echo $v['id'];?>'><button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4">
														Thêm vào giỏ
													</button>
													</a>
												</div>
                                            </div> <?php } ?>
										</div>
	
										<div class="block2-txt p-t-20">
											<a href="product-detail.html" class="block2-name dis-block s-text3 p-b-5">
												<big><?php echo $v['name']; ?></big>
												<b class='text-danger'><?php if($rate>0) echo "(-$rate%)" ?></b>
												<b class='text-danger'><?php if($amount[0]['total'] ==0) echo "(HẾT HÀNG)" ?></b>
											</a>
	
											<span class="block2-price m-text6 p-r-5">
												<?php 
												if($rate > 0)
												{
													?>
													<b style='text-decoration: line-through;'><?php echo number_format($v['price']); ?> VND</b> 
													<b class='text-danger'><?php echo number_format(round($v['price']*(100-$rate)/100 , -3	)); ?> VND</b> 
													<?php
												}
												else
												{
													?>
													<b class='text-danger'><?php echo number_format($v['price']); ?>VND</b> 
													<?php
												}
												?>
											</span>
										</div>
									</div>
								</div><?php
								}
							}
                            ?>                            
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Our product -->
	<section class="bgwhite p-t-45 p-b-58">
		<div class="container">
			<div class="sec-title p-b-22">
				<h3 style='font-family:arial' class="m-text5 t-center">
					KHUYẾN MẠI
				</h3>
			</div>

			<!-- Tab01 -->
			<div class="tab01">
				<!-- Nav tabs -->

				<!-- Tab panes -->
				<div class="tab-content p-t-35">
					<!-- - -->
					<div class="tab-pane fade show active" id="best-seller" role="tabpanel">
						<div class="row">
							<?php 
							$i=0;
                            $p=new product();
							foreach($p->getnew() as $k=>$v)
							{
								if($i<8)
								{ 
									$detail=new product_detail();
                                    $amount=$detail->getamountbyProduct($v['id']);
									$pro=new promotion();
									$pr=$pro->getallbyProduct($v['id']);
									if(count($pr)>=1)
									$rate=$pr[0]['rate'];
									else continue;
									?>
									<div class="col-sm-6 col-md-4 col-lg-3 p-b-50">
									<!-- Block2 -->
									<div class="block2">
										<div class="block2-img wrap-pic-w of-hidden pos-relative block2-labelsale">
											<img src="<?php echo base_url();?>image/product/<?php echo $v['id'];?>.png" alt="IMG-PRODUCT">
	
											<?php if($amount[0]['total'] !=0){?>
											<div class="block2-overlay trans-0-4">
												<div class="block2-btn-addcart w-size1 trans-0-4">
                                                    <!-- Button -->
													<a href='<?php echo base_url();?>gio-hang/insert/<?php echo $v['id'];?>'><button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4">
														Thêm vào giỏ
													</button>
													</a>
												</div>
                                            </div> <?php } ?>
										</div>
	
										<div class="block2-txt p-t-20">
											<a href="product-detail.html" class="block2-name dis-block s-text3 p-b-5">
												<big><?php echo $v['name']; ?></big>
												<b class='text-danger'><?php if($rate>0) echo "(-$rate%)" ?></b>
												<b class='text-danger'><?php if($amount[0]['total'] ==0) echo "(HẾT HÀNG)" ?></b>
											</a>
	
											<span class="block2-price m-text6 p-r-5">
												<?php 
												if($rate > 0)
												{
													?>
													<b style='text-decoration: line-through;'><?php echo number_format($v['price']); ?> VND</b> 
													<b class='text-danger'><?php echo number_format(round($v['price']*(100-$rate)/100 , -3	)); ?> VND</b> 
													<?php
												}
												else
												{
													?>
													<b class='text-danger'><?php echo number_format($v['price']); ?>VND</b> 
													<?php
												}
												?>
											</span>
										</div>
									</div>
								</div><?php
								}
								$i++;
							}
                            ?>                            
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php
$display=new display();
$midtitle=$display->searchbyName('middle title');
$midsmall=$display->searchbyName('middle small');
?>
	<section class="parallax0 parallax100" style="background-image: url(<?php echo base_url();?>image/homemiddle.png);">
		<div class="overlay0 p-t-190 p-b-200">
			<div class="flex-col-c-m p-l-15 p-r-15">
				<span class="m-text9 p-t-45 fs-20-sm">
					<?php if(count($midsmall)>0)  echo $midsmall['0']['content'];?>
				</span>
				<h3 class="l-text1 fs-35-sm">
				<?php if(count($midtitle)>0)  echo $midtitle['0']['content'];?>
				</h3>
			</div>
		</div>
	</section>

	

	<!-- Shipping -->
	<section class="shipping bgwhite p-t-62 p-b-46">
		
		</div>
	</section>
