<?php $voucher=new voucher();$vc=$voucher->getbyId($_SESSION['user_data']['cart']['voucher']);
	if($_SESSION['user_data']['cart']['amount']==0) redirect(base_url()."trang-chu");
	if($vc==array())
	{
		$vc[0]=array('rate'=>0);
	}
	?>
	<!-- Title Page -->
	<section class="bg-title-page p-t-40 p-b-50 flex-col-c-m" style="background-image: url(<?php echo base_url();?>image/title.png);">
		<h2 class="l-text2 t-center">
			<?php echo $title;?>
		</h2>
	</section>
	<!-- Cart -->
	<section class="cart bgwhite p-t-70 p-b-100">
		<div class="container">
			<!-- Cart item -->THÔNG TIN THANH TOÁN
            <?php 
            $user=new user();
			$u=$user->getbyUsername($_SESSION['user_data']['username']);
            ?>
			<form class='form-group' method='post' action='<?php echo base_url();?>order/insert'>
            <table class='table'>
<tr><td width=130>Họ tên:</td><td><b><input class='form-control bg-light' type='text' name='name' <?php if(count($u)>0){ ?> value='<?php echo $u['0']['name'];?>' <?php }?> placeholder="Họ tên" required></b></td><td width=130>Giới tính:</td>
                    <td><b><select name='gender' class='form-control bg-light'>
                    <option value=1 <?php if(count($u)>0){ if($u['0']['gender']==1) echo 'selected';}?>>Nam</option>
                    <option value=0 <?php if(count($u)>0){ if($u['0']['gender']==0) echo 'selected';}?>>Nữ</option>
                    </select></b></td></tr>
                <tr><td width=130>Ngày sinh:</td><td><b><input class='form-control bg-light' type='date' name='birth' <?php if(count($u)>0){ ?> value='<?php echo date('Y-m-d',strtotime($u['0']['birthday']));?>' <?php }?>  required></b></td><td width=150>
				Số chứng minh:</td><td><b><input class='form-control bg-light' type='number' name='id_card' <?php if(count($u)>0){ ?> value='<?php echo $u['0']['id_card'];?>' <?php }?> placeholder="Số chứng minh" required></b></td></tr>
                <tr><td width=130>Địa chỉ:</td><td colspan=3><b><input class='form-control bg-light' type='text' name='add' <?php if(count($u)>0){ ?> value='<?php echo $u['0']['address'];?>' <?php }?> placeholder="Địa chỉ" required></b></td></tr>
                <tr><td width=130>Số điện thoại:</td><td><b><input class='form-control bg-light' type='number' name='phone' <?php if(count($u)>0){ ?> value='<?php echo $u['0']['phone'];?>' <?php }?> placeholder="Số điện thoại" required></b></td><td width=130>
				Mail:</td><td><b><input class='form-control bg-light' type='text' name='mail' <?php if(count($u)>0){ ?> value='<?php echo $u['0']['email'];?>' <?php }?> placeholder="Mail" required></b></td></tr>
                <tr><td width=130>Loại thanh toán:</td>
                    <td><b><select name='type' class='form-control bg-light'>
                    <option value=1>Thanh toán trực tiếp khi nhận hàng</option>
                    <option value=2>Thanh toán qua cổng Ngân Lượng</option>
                    </select></b></td></tr>
                <tr><td colspan=4><input class='btn btn-light active' type='submit' value='Xác nhận thông tin thanh toán'></td></tr>         
            </table></form> 
            </div> 
            

			<!-- Total -->
			<div class="bo9 w-size18 p-l-40 p-r-40 p-t-30 p-b-38 m-t-30 m-r-0 m-l-auto p-lr-15-sm">
			<h5 class="m-text20 p-b-24">
					Đơn hàng
				</h5>
				
				<!--  -->
				<?php 
				$total=$_SESSION['user_data']['cart']['total'];
				foreach($_SESSION['user_data']['cart']['list'] as $v){
					$p=new product();
					$pd=$p->getbyId($v['id']);
					$pro=new promotion();
					$pr=$pro->getallbyProduct($v['id']);
					if(count($pr)>=1)
					$rate=$pr[0]['rate'];
					else $rate=0;
					?>
					<div class="flex-w flex-sb-m p-b-12">
					<span class="s-text18 w-size10 w-full-sm">
						<?php echo $pd[0]['name'];if($rate>0) echo "(-$rate%)"; ?>
					</span>
					<span class="m-text21 w-size10 w-full-sm">
						x <?php echo $v['amount'] ?>
					</span>
					<span class="m-text21 w-size10 w-full-sm">
						<?php echo number_format($pd[0]['price']); ?>
					</span>
				</div>
					<?php
				}
				?>
				<div class="flex-w flex-sb-m p-b-12">
					<span class="s-text18 w-size10 w-full-sm">
						Tổng
					</span>
					<span class="m-text21 w-size10 w-full-sm">
						
					</span>
					<span class="m-text21 w-size10 w-full-sm">
					<?php echo number_format($_SESSION['user_data']['cart']['total']);?>
					</span>
				</div>
								<!--  -->
				<div class="flex-w flex-sb-m p-t-26 p-b-12">
					<span class="m-text18 w-size10 w-full-sm">
						Voucher
					</span>


					<span class="m-text18 w-size10 w-full-sm">
						<?php if($_SESSION['user_data']['cart']['voucher']==0) echo "Không có";
						else {
							echo "Giảm giá ".$vc[0]['rate']."%";
						}
						?>
					</span>
				</div>
								<!--  -->
				<div class="flex-w flex-sb-m p-t-10 p-b-30">
					<span class="m-text22 w-size19 w-full-sm">
						Tổng tiền
					</span>

					<span class="m-text21 w-size20 w-full-sm">
						<?php echo number_format($total*(1-$vc[0]['rate']/100));?>VND
					</span>
				</div>

				<div class="size15 trans-0-4">
					<!-- Button -->
					
				</div>
			</div>
		</div>
	</section>