<?php 
$news=new news();
$ds=$news->getall();
?>
<!-- Title Page -->
<section class="bg-title-page p-t-40 p-b-50 flex-col-c-m" style="background-image: url(<?php echo base_url();?>image/title.png);">
		<h2 class="l-text2 t-center">
			Tin tức
		</h2>
	</section>

	<!-- content page -->
	<section class="bgwhite p-t-60">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-lg-9 p-b-75">
                
					<div class="p-r-50 p-r-0-lg">
                    <?php foreach($ds as $k=>$v){ ?>
						<!-- item blog -->
						<div class="item-blog p-b-80">
							<a href="<?php echo base_url();?>tin-tuc/<?php echo $v['id'];?>" class="item-blog-img pos-relative dis-block hov-img-zoom">
								<img src="<?php echo base_url();?>image/tin-tuc<?php if(file_exists("./image/tin-tuc-".$v['id'].".png")) echo "-".$v['id']; ?>" alt="IMG-NEWS">

								<span class="item-blog-date dis-block flex-c-m pos1 size17 bg4 s-text1">
                                    <?php $d=new datetime($v['created_at']);
                                    echo $d->format('d/m')." - ".$d->format('H:m');?>
								</span>
							</a>

							<div class="item-blog-txt p-t-33">
								<h4 class="p-b-11">
									<a href="<?php echo base_url();?>tin-tuc/<?php echo $v['id'];?>" class="m-text24">
										<?php echo $v['title'];?>
									</a>
								</h4>

								<div class="s-text8 flex-w flex-m p-b-21">
									<span>
										Người đăng <?php echo $v['username'];?>
									</span>
								</div>
							</div>
						</div>
                <?php } ?>
						
					</div>

				</div>

				<div class="col-md-4 col-lg-3 p-b-80">
					<div class="rightbar">

						<!-- Categories -->
						<h4 class="m-text23 p-t-56 p-b-34">
							Danh mục
						</h4>

						<ul>
                        <?php $type=new product_type();
                        foreach($type->getallParent() as $k=>$v){
                            ?>
                            <li class="p-t-6 p-b-8 bo6">
								<a href="<?php echo base_url();?>danh-sach-san-pham?type=<?php echo $v['id']; ?>" class="s-text13 p-t-5 p-b-5">
									<?php echo $v['name'];?>
								</a>
							</li>
                            <?php
                        }?>
						</ul>

						<!-- Featured Products -->
						<h4 class="m-text23 p-t-65 p-b-34">
							Sản phẩm mới
						</h4>

						<ul class="bgwhite">
                        <?php $pr=new product();
                        foreach($pr->getnew() as $k=>$v){
                            if($k <5){
                            ?>
                            <li class="flex-w p-b-20">
								<a href="<?php echo base_url()?>san-pham/<?php echo $v['id'];?>" class="dis-block wrap-pic-w w-size22 m-r-20 trans-0-4 hov4">
									<img src="<?php echo base_url();?>image/product/<?php echo $v['id'];?>" alt="IMG-PRODUCT">
								</a>

								<div class="w-size23 p-t-5">
									<a href="<?php echo base_url()?>san-pham/<?php echo $v['id'];?>" class="s-text20">
										<?php echo $v['name'];?>
									</a>

									<span class="dis-block s-text17 p-t-6">
                                    <?php echo number_format($v['price']);?> vnd
									</span>
								</div>
							</li>
                            <?php
                        } }?>
														
						</ul>

						
					</div>
				</div>
			</div>
		</div>
	</section>