	<?php 
	if($_SESSION['user_data']['cart']['amount']==0) redirect(base_url()."trang-chu");
	if(!isset($_SESSION['user_data']['cart']['voucher'])) $_SESSION['user_data']['cart']['voucher']=0;
	$voucher=new voucher();$vc=$voucher->getbyId($_SESSION['user_data']['cart']['voucher']);
	if($vc==array())
	{
		$vc[0]=array('rate'=>0);
	}
	?>
	<!-- Title Page -->
	<section class="bg-title-page p-t-40 p-b-50 flex-col-c-m" style="background-image: url(<?php echo base_url();?>image/title.png);">
		<h2 class="l-text2 t-center">
			<?php echo $title;?>
		</h2>
	</section>
	<form id='form1' method='post' action='<?php echo base_url();?>gio-hang/update'></form>
	<!-- Cart -->
	<section class="cart bgwhite p-t-70 p-b-100">
		<div class="container">
			<!-- Cart item -->
			<div class="container-table-cart pos-relative">
				<div class="wrap-table-shopping-cart bgwhite">
					<table class="table-shopping-cart">
						<tr class="table-head">
							<th class="column-1"></th>
							<th class="column-2">SẢN PHẨM</th>
							<th class="column-3">GIÁ (VND)</th>
							<th class="column-4 p-l-70">SỐ LƯỢNG</th>
							<th class="column-5">TỔNG TIỀN (VND)</th>
						</tr>
                        <?php foreach($_SESSION['user_data']['cart']['list'] as $v){
                            $p=new product();
                            $pd=$p->getbyId($v['id']);
                            ?>
                            <tr class="table-row">
							<td class="column-1">
								<div class="cart-img-product b-rad-4 o-f-hidden">
									<img src="<?php echo base_url();?>image/product/<?php echo $v['id'];?>" alt="IMG-PRODUCT">
								</div>
							</td>
							<td class="column-2"><?php echo $pd[0]['name'];?></td>
							<td class="column-3"><?php echo number_format($pd[0]['price']);?></td>
							<td class="column-4">
								<div class="flex-w bo5 of-hidden w-size17">
									<button class="btn-num-product-down color1 flex-c-m size7 bg8 eff2">
										<i class="fs-12 fa fa-minus" aria-hidden="true"></i>
									</button>

									<input form='form1' class="size8 m-text18 t-center num-product" type="number" name="num-product[]" value="<?php echo $v['amount'];?>" readonly>

									<button class="btn-num-product-up color1 flex-c-m size7 bg8 eff2">
										<i class="fs-12 fa fa-plus" aria-hidden="true"></i>
									</button>
								</div>
							</td>
							<td class="column-5"><?php echo number_format($v['amount']*$pd[0]['price']); ?></td>
						</tr>
                            <?php
                        } ?>
					</table>
				</div>
			</div>

			<div class="flex-w flex-sb-m p-t-25 p-b-25 bo8 p-l-35 p-r-60 p-lr-15-sm">
				<div class="flex-w flex-m w-full-sm">
                    <form action='<?php echo base_url();?>gio-hang/updatevoucher'>
					<div class="size11 bo4 m-r-10">
						<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="voucher" placeholder="voucher" value='<?php echo $_SESSION['user_data']['cart']['voucher'];?>'>
					</div>

					<div class="size12 trans-0-4 m-t-10 m-b-10 m-r-10">
						<!-- Button -->
						<button class="flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4">
							Thêm mã voucher
						</button>
                    </div>
                    </form>
				</div>

				<div class="size10 trans-0-4 m-t-10 m-b-10">
					<!-- Button -->
					<button  form='form1' type='submit' class="flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4">
						Cập nhật giỏ hàng
					</button>
				</div>
			</div>

			<!-- Total -->
			<div class="bo9 w-size18 p-l-40 p-r-40 p-t-30 p-b-38 m-t-30 m-r-0 m-l-auto p-lr-15-sm">
				<h5 class="m-text20 p-b-24">
					Đơn hàng
				</h5>
				
				<!--  -->
				<?php 
				$total=$_SESSION['user_data']['cart']['total'];
				foreach($_SESSION['user_data']['cart']['list'] as $v){
					$p=new product();
					$pd=$p->getbyId($v['id']);
					$pro=new promotion();
					$pr=$pro->getallbyProduct($v['id']);
					if(count($pr)>=1)
					$rate=$pr[0]['rate'];
					else $rate=0;
					?>
					<div class="flex-w flex-sb-m p-b-12">
					<span class="s-text18 w-size10 w-full-sm">
						<?php echo $pd[0]['name'];if($rate>0) echo "(-$rate%)"; ?>
					</span>
					<span class="m-text21 w-size10 w-full-sm">
						x <?php echo $v['amount'] ?>
					</span>
					<span class="m-text21 w-size10 w-full-sm">
						<?php echo number_format($pd[0]['price']); ?>
					</span>
				</div>
					<?php
				}
				?>
				<div class="flex-w flex-sb-m p-b-12">
					<span class="s-text18 w-size10 w-full-sm">
						Tổng
					</span>
					<span class="m-text21 w-size10 w-full-sm">
						
					</span>
					<span class="m-text21 w-size10 w-full-sm">
					<?php echo number_format($_SESSION['user_data']['cart']['total']);?>
					</span>
				</div>
								<!--  -->
				<div class="flex-w flex-sb-m p-t-26 p-b-12">
					<span class="m-text18 w-size10 w-full-sm">
						Voucher
					</span>


					<span class="m-text18 w-size10 w-full-sm">
						<?php if($_SESSION['user_data']['cart']['voucher']==0) echo "Không có";
						else {
							echo "Giảm giá ".$vc[0]['rate']."%";
						}
						?>
					</span>
				</div>
								<!--  -->
				<div class="flex-w flex-sb-m p-t-10 p-b-30">
					<span class="m-text22 w-size19 w-full-sm">
						Tổng tiền
					</span>

					<span class="m-text21 w-size20 w-full-sm">
						<?php echo number_format($total*(1-$vc[0]['rate']/100));?>VND
					</span>
				</div>

				<div class="size15 trans-0-4">
					<!-- Button -->
					<a href='<?php echo base_url();?>thanh-toan'>
					<button class="flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 ">			
					TIẾN HÀNH THANH TOÁN
					</button></a>
				</div>
			</div>
		</div>
	</section>