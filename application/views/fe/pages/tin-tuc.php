<?php 
$news=new news();
$tt=$news->getbyId($id);
?>
	<!-- content page -->
	<section class="bgwhite p-t-60 p-b-25">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-lg-9 p-b-80">
					<div class="p-r-50 p-r-0-lg">
						<div class="p-b-40">
							<div class="blog-detail-img wrap-pic-w">
								<img src="<?php echo base_url();?>image/tin-tuc<?php if(file_exists("./image/tin-tuc-".$tt[0]['id'].".png")) echo "-".$tt[0]['id']; ?>" alt="IMG-BLOG">
							</div>

							<div class="blog-detail-txt p-t-33">
								<h4 class="p-b-11 m-text24">
                                <?php echo $tt[0]['title'];?>
								</h4>

								<div class="s-text8 flex-w flex-m p-b-21">
									<span>
                                    Người đăng <?php echo $tt[0]['username'];?>
										<span class="m-l-3 m-r-6">|</span>
									</span>

									<span>
                                        <?php echo $tt[0]['created_at'];?>
										<span class="m-l-3 m-r-6">|</span>
									</span>
								</div>


								<p class="p-b-25">
									<?php echo $tt[0]["content"];?>
								</p>
							</div>

							
						</div>
					</div>
				</div>

				<div class="col-md-4 col-lg-3 p-b-80">
					<div class="rightbar">

						<!-- Categories -->
						<h4 class="m-text23 p-t-56 p-b-34">
							Danh mục
						</h4>

						<ul>
                        <?php $ca=new product_type();
                        foreach($ca->getallParent() as $k=>$v){
                            ?>
                            <li class="p-t-6 p-b-8 bo6">
								<a href="<?php echo base_url();?>danh-sach?category=<?php echo $v['id']; ?>" class="s-text13 p-t-5 p-b-5">
									<?php echo $v['name'];?>
								</a>
							</li>
                            <?php
                        }?>
						</ul>

						<!-- Featured Products -->
						<h4 class="m-text23 p-t-65 p-b-34">
							Sản phẩm mới
						</h4>

						<ul class="bgwhite">
                        <?php $pr=new product();
                        foreach($pr->getall() as $k=>$v){
                            if($k <5){
                            ?>
                            <li class="flex-w p-b-20">
								<a href="<?php echo base_url()?>san-pham/<?php echo $v['id'];?>" class="dis-block wrap-pic-w w-size22 m-r-20 trans-0-4 hov4">
									<img src="<?php echo base_url();?>image/product/<?php echo $v['id'];?>" alt="IMG-PRODUCT">
								</a>

								<div class="w-size23 p-t-5">
									<a href="<?php echo base_url()?>san-pham/<?php echo $v['id'];?>" class="s-text20">
										<?php echo $v['name'];?>
									</a>

									<span class="dis-block s-text17 p-t-6">
                                    <?php echo number_format($v['price']);?> vnd
									</span>
								</div>
							</li>
                            <?php
                        } }?>
														
						</ul>

						
					</div>
				</div>
			</div>
		</div>
	</section>
