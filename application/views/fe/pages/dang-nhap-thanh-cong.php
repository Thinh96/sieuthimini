<div class='container-fluid' style='min-height:500px;background:url(<?php echo base_url();?>image/login_background) no-repeat; background-size:1400px 500px;background-position:center;'>
    <h2 class='text-primary' style='text-align:center'>Đăng nhập thành công</h2>
    <div class='row text-light' style='width:300px;border-radius:5px;background:rgba(0,0,0,0.75);padding:10px;margin:10px auto;'>
    <div style='margin:5px' class='col-md-6'>
        <a href='<?php echo base_url();?>'><button class='btn btn-danger'>Trang chủ</button></a><br>
    </div>
    <div  style='margin:5px' class='col-md-6'>
        <a href='<?php echo base_url();?>danh-sach-san-pham'><button class='btn btn-danger'>Xem danh sách sản phẩm</button></a><br>
    </div>
    <div  style='margin:5px' class='col-md-6'>
        <a href='<?php echo base_url();?>gio-hang'><button class='btn btn-danger'>Xem giỏ hàng</button></a><br>
    </div>
    <?php if($_SESSION['user_data']['role']>2)
    {
        ?>
        <div  style='margin:5px' class='col-md-6'>
        <a href='<?php echo base_url();?>be'><button class='btn btn-primary'>Trang quản lý</button></a><br>
    </div>
        <?php
    }?>
    </div>
</div>