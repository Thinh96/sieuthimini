<?php 
$user=new user();
$u=$user->getbyUsername($_SESSION['user_data']['username']);
?>
<!-- Title Page -->
    <section class="bg-title-page p-t-40 p-b-50 flex-col-c-m" style="background-image: url(<?php echo base_url();?>image/title.png);">
		<h2 class="l-text2 t-center">
			<?php echo $title;?>
		</h2>
	</section>
    <section class="cart bgwhite p-t-70 p-b-100">
		<div class="container">
        <div class='row'>
            <div class='col-md-auto'><a href='<?php echo base_url();?>thong-tin-ca-nhan'><button class='btn btn-light'>Thông tin cá nhân</button></a></div>
            <div class='col-md-auto'><a href='<?php echo base_url();?>danh-sach-don-hang'><button class='btn btn-light active'>Danh sách đơn hàng</button></a></div>
<?php if($_SESSION['user_data']['role']>3){ ?><div class='col-md-auto'><a href='<?php echo base_url();?>be'><button class='btn btn-light'>Trang quản lý</button></a></div><?php } ?>
<div class='col-md-auto'><a href='<?php echo base_url();?>dang-nhap/logout'><button class='btn btn-light'>Đăng xuất</button></a></div>
            </div><br>  
			<!-- Cart item -->
			<div class="container-table-cart pos-relative">
            <h2>DANH SÁCH ĐƠN HÀNG</h2>
            <table class='table table-striped'>
            <tr>
            <th>#</th>
            <th>Mã đơn hàng</th>
            <th>Tổng tiền</th>
            <th>Loại</th>
            <th>Trạng thái</th>
            </tr>
            <?php 
            $od=new order();
            $dsod=$od->getallUsername($_SESSION['user_data']['username']);
            foreach($dsod as $k=>$v)
            {
                $voucher=new voucher();
                $vc=0;
                if(count($voucher->getbyId($v['voucher']))>0)
                $vc=$v['voucher'];
                ?>
                <tr>
                <td><?php echo $k+1;?></td>
                <td><?php echo $v['id'];?></td>
                <td><?php echo number_format($v['total']*(1-$vc/100));?></td>
                <td><?php if($v['type']==1) echo "Trả trước"; else echo "Trả sau";?></td>
                <td><?php 
                switch($v['status'])
                {
                    case 0: echo "Hủy";break;
                    case 1: echo "Chờ xử lý";break;
                    case 2:
                            $display=new display();
                            $pay_mail=$display->searchbyName('mail');
                            $pay_mail=$pay_mail[0]['content'];
                            echo "Chưa thanh toán";
                            echo "<a style='color:red' href='https://www.nganluong.vn/button_payment.php?receiver=".$pay_mail."&product_name=".$v['id']."&price=".$v['total']."&return_url=".base_url()."return%47".$v['id']."&comments='> thanh toán lại</a>";
                            break;
                    case 3: echo "Vận chuyển";break;
                }
                ?>                
                </td>
                </tr>
                <?php
            }
            ?>
            </table></div>        
			
        </div>
    </section>