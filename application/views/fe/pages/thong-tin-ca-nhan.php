<?php 
$user=new user();
$u=$user->getbyUsername($_SESSION['user_data']['username']);
?>
<!-- Title Page -->
    <section class="bg-title-page p-t-40 p-b-50 flex-col-c-m" style="background-image: url(<?php echo base_url();?>image/title.png);">
		<h2 class="l-text2 t-center">
			<?php echo $title;?>
		</h2>
	</section>
    <section class="cart bgwhite p-t-70 p-b-100">
		<div class="container">
        <div class='row'>
            <div class='col-md-auto'><a href='<?php echo base_url();?>thong-tin-ca-nhan'><button class='btn btn-light active'>Thông tin cá nhân</button></a></div>
            <div class='col-md-auto'><a href='<?php echo base_url();?>danh-sach-don-hang'><button class='btn btn-light'>Danh sách đơn hàng</button></a></div>
<?php if($_SESSION['user_data']['role']>=3){ ?><div class='col-md-auto'><a href='<?php echo base_url();?>be'><button class='btn btn-light'>Trang quản lý</button></a></div><?php } ?>
<div class='col-md-auto'><a href='<?php echo base_url();?>dang-nhap/logout'><button class='btn btn-light'>Đăng xuất</button></a></div>
            </div><br>  
			<!-- Cart item -->
			<div class="container-table-cart pos-relative">
            <h2>THÔNG TIN THÀNH VIÊN</h2>
            <table class='table'>
                <tr><td width=130>Tài khoản:</td><td><b><?php echo $_SESSION['user_data']['username'];?></b></td><td colspan=2></td></tr>
                <tr><td width=130>Họ tên:</td><td><b><?php echo $u['0']['name'];?></b></td><td width=130>Giới tính:</td><td><b><?php if($u['0']['gender']==1) echo 'Nam'; else echo 'Nữ';?></b></td></tr>
                <tr><td width=130>Ngày sinh:</td><td><b><?php echo date('d/m/Y',strtotime($u['0']['birthday']));?></b></td><td width=150>Số chứng minh:</td><td><b><?php echo $u['0']['id_card'];;?></b></td></tr>
                <tr><td width=130>Địa chỉ:</td><td colspan=3><b><?php echo $u['0']['address'];?></b></td></tr>
                <tr><td width=130>Số điện thoại:</td><td><b><?phpecho $u['0']['phone'];?></b></td><td width=130>Mail:</td><td><b><?php echo $u['0']['email'];?></b></td></tr>            
            </table>   </div> 
            <div class='row'>
            <div class='col-md-auto'><a href='<?php echo base_url();?>cap-nhat-thong-tin'><button class='btn btn-light'>Cập nhật thông tin</button></a></div>
            <div class='col-md-auto'><a href='<?php echo base_url();?>doi-mat-khau'><button class='btn btn-light'>Đổi mật khẩu</button></a></div>
            </div><br>          
			
        </div>
    </section>